---
id: contribute
title: Partager en retour & Contribuer
sidebar_label: Partager en retour
---

<style>
:root {
  --highlight: #f090b3;
  --links: #f090b3;
  --hover: #f2a5c1;
}
</style>

<img src="../../assets/universe/ecosystem.png"/>

# Pourquoi le fait de partager en retour est crucial pour notre futur

Salut, content de voir que tu es venu sur cette section de l’Academy. Ce sujet est essentiel pour la pérennité de Precious Plastic. Laisse moi t’expliquer la force de la communauté 💪. La plupart des développements publiés ici dans l’Academy viennent des quartiers généraux, où nous invitons des personnes de la communauté dans notre atelier et développons des nouvelles choses. C’est pour sûr une manière ludique et puissante de faire des développements. 

Mais cela requiert aussi un certain nombre de ressources (budget, nourriture, couchages, espace, etc.). Et de manière réaliste, nous ne serons jamais capables de tous vous inviter, experts du plastiques, ici dans notre bâtiment. Et avec une communauté grandissante à travers le monde et des membres toujours plus nombreux, nous (la communauté) découvrons beaucoup de choses de façon quotidienne, améliorations, bricolages de machine, systèmes de nettoyage, mécanismes de sûreté, etc. 

Nous avons besoin plus que jamais de partager nos connaissances ensemble, de manière centralisée. Pour que nous n'ayons pas à faire les mêmes erreurs et ainsi pouvoir apprendre de l’un l’autre.


## 5 raisons pour lesquelles vous devriez partager en retour

__1. 🏅Réputation digitale:__ Cela renforce votre crédibilité dans la communauté. Les gens tendent à aider davantage s’ils voient que vous contribuez.

__2. 🤝 Aider les autres:__ De façon assez claire, rien n’empêche les personnes de copier coller votre travail. Ce qui est fascinant, surtout si c’est reproduit avec des variations. En partageant simplement votre méthodologie et vos conseils, cela aide un paquet de personnes à apprendre et gagner en compétences. Dans les deux sens c’est très utile.

__3. 📝 Les connaissances restent pertinentes:__ Les personnes en sont à des stades différents de développement. Donc il est fort probable que certaines utilisent vos savoirs pour leur projet 3 ans après que vous les ayez publié. Cela demande un effort à un moment, mais continuera de prospérer et d’aider des personnes pour des années à venir - augmentant vos points de Karma!

__4. ⭐️ Vous avez déjà fait le plus dur:__ Quand on pense à écrire de la documentation, c’est qu’on a déjà fait le principal. Vous avez construit une machine, conçu un produit, inventé une amélioration… En gros 95% du boulot est terminé! Mais il reste ces derniers 5% pour partager le travail fraîchement abouti.

__5. 🙃 Cinq sonne mieux que quatre:__ Donc le point 5 est juste pour faire valoir cet argument.

<img src="../../assets/universe/how-to.png"/>


# Documentation centralisée: How-To’s

La documentation et le partage des connaissances sont en fait l'un des principaux défis du travail en open-source. Vous vous retrouvez facilement avec des connaissances éparpillées sur le web. Forums, Discord, Slack, Stackoverflow, commentaires, etc. Cela devient rapidement un désordre rendant très difficile de retrouver les bonnes informations sur le long terme. 

C'est pourquoi nous avons commencé à travailler sur les [how-tos](https://community.preciousplastic.com/how-to). Un endroit centralisé dans la communauté où les membres peuvent télécharger leurs conceptions, leçons, hacks, mises à jour, etc. Vous pouvez filtrer sur les tags pour trouver ce dont vous avez besoin et vous référer à d'autres how-to. C'est un endroit pour centraliser toutes les choses que nous apprenons en tant que communauté. Parce qu'il y a encore beaucoup de choses à découvrir en matière de recyclage du plastique. Nous avons donc besoin les uns des autres.

> Note:  Les tutoriels sont encore en cours de développement. De nombreuses fonctionnalités, comme les commentaires et la sauvegarde, sont encore manquantes. Mais plus vous l'utiliserez, plus il nous sera facile de recueillir des retours et d'apporter des améliorations. Aidez-nous à faire œuvre de pionnier et à améliorer la situation pour le reste du monde.

# Mentionnez-nous! #preciousplastic
Quand vous partagez vos créations avec le reste du monde, nous vous prions de mentionner ou d'épingler Precious Plastic. Pourquoi?

__Pour le crédit:__ Nous donnons tout en libre accès et permettons à des milliers de personnes dans le monde de gagner un peu d'argent avec des machines ou des produits de recyclage. Donc un petit merci sous forme de mention devrait être facile à donner en retour 🙃

__Pour la mise en avant:__ Nous sommes toujours en train de ratisser la communauté pour trouver des créations inspirantes à partager. Mais nous ne pouvons vous mettre en avant sur nos médias sociaux ou nos vidéos que si nous vous TROUVONS réellement et que nous voyons que vous êtes connecté à Precious Plastic. Alors que faire ? Utilisez le hashtag [#preciousplastic](https://www.instagram.com/explore/tags/preciousplastic/), ou mieux encore, mentionnez @realpreciousplastic, pour nous aider à vous trouver.

<br>
<br>
<br>

- ⭐️ Suivez notre [instagram](https://instagram.com/realpreciousplastic) pour avoir un aperçu des belles choses de notre communauté.
- ⭐️ Parcourez le [ Facebook](https://facebook.com/preciousplastic) de la communauté pour voir des choses réelles et brutes réalisées par notre armée mondiale.
- ⭐️ Visitez [ce chapitre](../../create/howto) de l'Académy pour voir, étape par étape, comment créer le meilleur mode d'emploi.
