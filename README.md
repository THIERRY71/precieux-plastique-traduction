# Notes diverses sur l'infrastructure Docusaurus et la traduction

## :warning: Différences avec la version originale One Army
- le dossier **ms56-tools** contient des scripts utilisés localement, il peut être supprimé.
- le dossier **docs/assets-source** contient les sources des SVG sans transformation en chemin des SVG.

## Méthode de traduction
   Pour assurer la cohérence de l'activité collective, la procédure suivante est recommandée :
	
1. Choisir le fichier souhaité (par exemple **docs/guides/platform.md**)
2. Publier un ticker avec pour titre "je travaille sur docs/guides/platform.md)
3. Modifier le fichier
4. Une fois la modification terminée, réaliser la demande de fusion en appliquant la modification ("Commit") sous une branche nommée (ici) "docs-guides-platform.md"

## Conventions
Pour assurer une certaines homogénéïté des traductions, il est conseillé de suivre quelques règles de style. (Je me suis largement inspiré des règles en vigueur dans le groupe de traduction de Python)

### Style

Une bonne traduction est une traduction qui transcrit fidèlement l'idée originelle en français, sans rien ajouter ni enlever au fond, tout en restant claire, concise et agréable à lire. Les traductions mot-à-mot sont à proscrire et il est permis — même conseillé — d'intervertir des propositions ou de réarranger des phrases de la documentation anglaise, si le rythme l'exige. Il faut aussi chercher des équivalents français aux termes techniques et aux idiotismes rencontrés, et prendre garde aux anglicismes.

### Utilisation du conditionnel
La version originale est très polie envers le lecteur ; elle lui intime rarement des obligations, préférant employer « you should ». Cependant, en français, il est d'usage d'être plus direct pour être correctement compris : « vous devez ». Vous devriez est en effet généralement compris comme quelque chose dont l'on peut de temps en temps se passer, alors que c'est très rarement le cas pour les « you should » de cette documentation. De la même manière, « can » est souvent mieux traduit sans introduire de notion de possibilité, en particulier quand la phrase est à la voix passive ; la phrase « these objects can be accessed by… » se traduit mieux par « on accède à ces objets en… ».

### Utilisation du masculin
Dans un souci de lisibilité et en accord avec la préconisation de l'Académie française, nous utilisons le masculin pour indiquer un genre neutre. Par exemple : l'utilisateur ou le lecteur.

## Ressources pour aider à la traduction
- Le Canal #traduction sur le serveur Discord de la communauté francophone Precious Plastic.
- les `glossaires et dictionnaires de traduc.org <https://traduc.org/Glossaires_et_dictionnaires>`, en particulier le  `grand dictionnaire terminologique <http://gdt.oqlf.gouv.qc.ca/>` de l'Office québécois de la langue française,
- Wikipédia. En consultant un article sur la version anglaise, puis en basculant sur la version francaise pour voir comment le sujet de l'article est traduit ;
- le `guide stylistique pour le français de localisation des produits Sun <https://web.archive.org/web/20160821182818/http://frenchmozilla.org/FTP/TEMP/guide_stylistique_December05.pdf>` donne beaucoup de conseils pour éviter une traduction trop mot à mot ;
- `Petites leçons de typographie <https://jacques-andre.fr/faqtypo/lessons.pdf>`,
  résumé succinct de typographie, utile pour apprendre le bon usage des
  majuscules, des espaces, etc.

L'utilisation de traducteurs automatiques comme `DeepL <https://www.deepl.com/>` ou semi-automatiques comme `reverso <https://context.reverso.net/traduction/anglais-francais/>` est proscrite.
Les traductions générées sont très souvent à retravailler, ils ignorent les règles énoncées sur cette page et génèrent une documentation au style très « lourd ». 

# Caractères spéciaux et typographie

## La touche de composition

Cette `touche <https://fr.wikipedia.org/wiki/Touche_de_composition>`,
absente par défaut des claviers, permet de saisir des
caractères spéciaux en combinant les caractères déjà présents sur le
clavier. C'est à l'utilisateur de définir la touche de composition.

Avec une touche de composition, vous pouvez utiliser les
compositions suivantes :

- :kbd:`Compose < <` donne ``«``
- :kbd:`Compose > >` donne ``»``
- :kbd:`Compose SPACE SPACE` donne une espace insécable
- :kbd:`Compose . . .` donne ``…``

Comme vous l'avez noté, presque toutes les compositions sont intuitives,
vous pouvez donc en essayer d'autres et elles devraient tout
simplement fonctionner :

- :kbd:`Compose C =` donne ``€``
- :kbd:`Compose 1 2` donne ``½``
- :kbd:`Compose ' E` donne ``É``
- etc.

## Comment définir la touche de composition ?

Cela dépend de votre système d'exploitation et de votre clavier.

⇒ Sous Linux, Unix et \*BSD (tel OpenBSD), vous pouvez la configurer à l'aide de
l'outil graphique de configuration de votre clavier ou avec
``dpkg-reconfigure keyboard-configuration``
(pour `Ubuntu <https://help.ubuntu.com/community/ComposeKey>`_ ou Debian
et distributions assimilées).

À tout le moins, vous pouvez configurer votre fichier *~/.Xmodmap* pour
ajouter l'équivalent de :

```shell

    # key Compose
    keycode 115 = Multi_key
```

Utilisez ``xev`` pour connaitre la bonne correspondance de la touche que vous
voulez assigner !

Ensuite, dans votre fichier *~/.xsession*, ajoutez :

```shell

    # Gestion des touches clavier
    xmodmap $HOME/.Xmodmap
```

⇒ Sous X, avec un bureau graphique, tel que Gnome, ou Xfce, il faut aller
modifier dans les « Paramètres » → « Clavier » → « Disposition » →
« Touche composée ». Pour finir, redémarrez votre session.

⇒ Sous Windows, vous
pouvez utiliser `wincompose <https://github.com/SamHocevar/wincompose>`_.

## Le cas de « --- », « -- »,  « ... »

La version anglaise utilise les
`smartquotes <http://docutils.sourceforge.net/docs/user/smartquotes.html>`_,
qui fonctionnent en anglais, mais causent des problèmes dans d'autres langues.
Nous les avons donc désactivées (voir #303) dans la version française.

Les *smartquotes* sont normalement responsables de la transformation de
``--`` en *en-dash* (``—``), de ``---`` en *em-dash* (``—``), et de
``...`` en *ellipses* ``…``.

⇒ Si vous voyez :
| « -- » ou « --- » : faites :kbd:`Compose - - -`
| « ... » : faites :kbd:`Compose . . .`

## Le cas de « "…" »

Les guillemets français ``«`` et ``»`` ne sont pas identiques aux
guillemets anglais ``"``. Cependant, Python utilise les guillemets
anglais comme délimiteurs de chaîne de caractères. Il convient donc de
traduire les guillemets mais pas les délimiteurs de chaîne.

⇒ Si vous voyez :
| « "…" » : faites :kbd:`Compose < <` ou :kbd:`Compose > >`

## Le cas de « :: » (À SUPPRIMER C'EST DU RST)

| Du point de vue du langage *reStructuredText* (ou *rst*) utilisé dans la
  documentation nous voyons soit « bla bla:: », soit « bla bla. :: ».
| ``::`` collé à la fin d'un mot signifie « affiche ``:`` et introduit un bloc de code »,
  mais un ``::`` après une espace signifie « introduit juste un bloc de code ».

En français, nous mettons une espace insécable devant nos deux-points, comme :
« Et voilà : ».

⇒ Traduisez ``mot deux-points deux-points`` par
``mot espace-insécable deux-points deux-points``.

Pour saisir une espace insécable faites :kbd:`Compose SPACE SPACE`

## Les doubles-espaces

La documentation originale comporte beaucoup de doubles-espaces.
Cela se fait en anglais, mais pas en français. De toute manière,
ils passent ensuite à une moulinette et le rendu des espaces est délégué
au HTML et au PDF, qui n'en tiennent pas compte.
Nous avons décidé de ne rien changer pour les doubles-espaces
coté traduction : nous ne les retirons pas et ce n'est pas grave
si des traducteurs en retirent par accident.

## Les énumérations

Chaque paragraphe d'une énumération introduite par un deux-point
doit se terminer par un point-virgule (bien entendu précédé d'une
espace insécable) quelle que soit sa ponctuation interne. Seul le dernier
paragraphe de l'énumération s'achève par un point ou, si la phrase
continue après l'énumération, une virgule. Si l'un des paragraphes est
lui-même une énumération, chacun des sous-paragraphes se termine par
une virgule et le dernier par un point-virgule.

Par exemple :

- le premier paragraphe de l'énumération ;
- le deuxième paragraphe, lui-aussi une énumération :

  - premier sous-paragraphe,
  - second sous-paragraphe ;

- le dernier paragraphe.

## Notes sur l'usage de Docusaurus
### Installation

1. Installer Node  
   L'infrastucture Docusaurus repose sur le langage de programmation **NodeJS**.  Pour l'installer, suivre les instruction données sur https://nodejs.org/en/download/

2. Installer Yarn  
   L'infrastructure Docusaurus repose sur le gestionnaire de paquet **Yarn**. Pour l'installer, suivre les instruction données sur https://yarnpkg.com/lang/en/docs/install

3. Option : installer un dépôt local
   Utiliser votre client git favorit (voir [client](https://git-scm.com/download/gui) ou  
   `git clone git@framagit.org:THIERRY71/precieux-plastique-traduction.git`
4. Installler les dépendances de l'infrastruction

```
cd academy/website

yarn install
```

### Lancement d'un serveur local

```
cd website 
yarn start
```

La commande ci-dessus lance automatiquement un serveur de développement local qui prend en charge dynamiquement tout changement effectué dans le navigateur web [à reformuler].
Tout changement fait devrait induire une recompilation et un rafraîchissement de la fenêtre du navigateur.

### Compilation des fichiers statiques
```
cd website
yarn run build
```
Les fichiers statiques sont placés dans le dossier **build**.
Pour tester rapidement, on peut utiliser Python
```
cd build
python3 -m http.server 127.0.0.1 <PORT>
```

# Traduction des ressources («docs/assets»)
Afin de faciliter la traduction dans d'autres langues, chaque fichier ressource **docs/assets** sera dupliqué sous une forme facilement éditable dans **docs/assets-sources** en conservant la même hiérarchie de dossiers.
Par forme facilement éditable, on peut citer :
- pour les **fichier SVG contenant du texte convertis en chemin** => ajouter un fichier SVG contenant le même texte sous forme de texte (sans conversion en chemin).

