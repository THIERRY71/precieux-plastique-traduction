---
id:intro
title:Intro
sidebar_label:Hello!
---
<div class="videocontainer"> <iframe width="800" height="400" src="https://www.youtube.com/embed/dP1s7viFZHY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </div>

<style> :root { --highlight: #ffe084; --links: #29bbe3; --hover: rgb(131, 206, 235); } </style>

# Bienvenue à la Precious Plastic Academy!

<div class="videoChapters"> <div class="videoChaptersMain">

### C’est génial de voir que vous êtes intéressé par le recyclage du plastique. Bienvenue parmis nous!

Au fil du temps, nous avons appris plein de choses sur la valorisation et la réutilisation des déchets plastiques. Pour les partager, nous avons créé des vidéos, des didacticiels et des contenus sur ces thèmes.

Ici sont présentées les bases: quels sont les différents types de plastique? Comment les distinguer les uns des autres? Comment les collecter, les trier? Quelles sont les machines utilisables, achetables voire constructibles? Comment devenir un membre actif de la communauté Precious Plastic? Et bien plus encore...

Des kits téléchargeables et des modèles sont également disponibles en libre accès. Ainsi, après avoir tout lu, tout est prêt pour démarrer un espace de travail et de recyclage n’importe où dans le monde.

</div> <div class="videoChaptersSidebar">

### Video Chapters

* 00:00 Introduction
* 00:42 Ce que vous allez apprendre
* 01:40 Aide & Feedback
* 02:01 Donation
* 02:30 Diffuser le projet

</div> </div>

![Open Source](assets/Intro/opensource.svg)

## Mais attendez - Pourquoi avoir fait cela? Et pourquoi est-ce gratuit?

Precious Plastic souhaite montrer les incroyables possibilités des déchets plastiques afin d'éliminer la pollution qu’ils engendrent, de réduire leur production et de réemployer ces matériaux tout en offrant de meilleurs moyens de subsistance aux populations du monde entier. Precious Plastic est avant tout un outil culturel pour changer la façon dont la société perçoit le plastique.

Tout le plastique qui nous entoure est une ressource, pas un déchet. C'est une matière aux nombreuses possibilités, disponible partout dans le monde et qui, s'il est traité correctement, peut être réemployé comme matériau neuf et précieux. Cette matière peut devenir une source de revenus ou un outil éducatif pour chacun d’entre nous, tout en s'assurant qu'elle ne pollue plus notre environnement.

<img style="padding: 50px 0" src="../assets/Intro/PP_universe.svg" width="500px"/>

Nous avons créé des machines, de la documentation, des didacticiels vidéo, des campagnes et une plate-forme pour permettre, partout dans tout le monde, de commencer à travailler avec les déchets plastiques d’abord localement mais tout en essayant de décentraliser et développer des technologies, des infrastructures et des connaissances de recyclage du plastique sur chaque territoire. Pour cela, nous fournissons des notices sous licence libre (« open source ») pour les machines, les moules et les consommables, ainsi que des modèles commerciaux et des outils pour configurer et gérer un espace de travail.

## Open source & Licence

Nous partageons tout sous licences libres pour que tout le monde puisse en bénéficier. Cela signifie que tous les savoirs, savoir-faire, processus, méthodes et outils sont disponibles en ligne et gratuitement à tout moment. Cette philosophie du Libre est au cœur de Precious Plastic et façonne la plupart des décisions au sein de notre équipe. Nous pensons que la collaboration est plus forte que la concurrence et que le problème plastique ne peut être résolu que collectivement.

Différentes parties de ce projet se trouve sous différentes conditions de licence

* **Contenu:** Tout notre contenu est sous licence Creative Commons Attribution - Sharealike International 4.0. Vous pouvez lire un résumé de celle-ci [ici](https://creativecommons.org/licenses/by-sa/4.0/), ou aller voir la [licence complète](https://creativecommons.org/licenses/by-sa/4.0/legalcode).
* **Code:** La licence pour le code du site de l'Academy est [GPL-3.0 License](https://github.com/ONEARMY/academy/blob/master/LICENSE).

## Soutenir & devenir impliqué

Si vous êtes dans une position où vous pouvez apporter de l'aide, il existe de multiples manières de <a href="https://support.preciousplastic.com/">soutenir Precious Plastic.</a>

<p class="note">Envie d’en savoir plus ou partager des savoirs et savoir-faire ? Il se trouve que nous avons créé un espace <a href="https://discordapp.com/invite/cGZ5hKP">Discord</a> pour discuter de tout ce que nous faisons ici. La section “**Questions**” dans la barre latérale du site contient toutes les informations pour trouver où échanger avec des personnes dans le monde entier.</p>

Ok, allons-y!
