---
id: sheetpressrun
title: Travailler avec la Presse à Plaques
sidebar_label: - l'Utiliser
---

<style>
:root {
  --highlight: #f29094;
  --hover: #f29094;
}
</style>
<div class="videocontainer">

![Sheetpress run](assets/build/sheetpress-run.jpg)

</div>

# Utilisation et Maintenance de la Presse à Plaques

Youpi! Vous vous êtes construit une Presse à Plaques! Maintenant il est temps de s’amuser à faire des plaques. Vous trouverez les explications ci-dessous, à propos du procédé idéal, des plages de température à utiliser, etc. mais aussi des informations pratiques à prendre en compte telles que la maintenance.

# 🏃‍♀️ Fabriquer une plaque!

## Utilisation de la Presse à Plaques uniquement

1. Vérifier que la machine est éteinte, et que le bouton d’arrêt d’urgence est désenclenché avant de la brancher. Allumer ensuite l’interrupteur principal.

2. Vérifier la température requise pour fondre le type de plastique à utiliser, et régler le contrôleur PID en adéquation.

3. Fermer les plaques de pressage à l’aide du cric et attendre qu’elles chauffent.

4. Pendant le temps d’attente, peser la quantité de plastique nécessaire. Voir la Datasheet pour les références.

5. Quand la machine a atteint la température désirée, ouvrir les plaques en relaĉhant la pression du cric.

6. Placer la plaque inférieure du moule sur la plaque de chauffe inférieure et appliquer une couche d’huile de silicone. Veiller à retirer tout résidu de la plaque précédente.

7. Placer le cadre du moule au centre de la plaque et appliquer une couche d’huile de silicone sur le haut du moule.

8. Charger le moule avec des copeaux de plastique.

9. Étaler le plastique équitablement sur l’ensemble de la surface à l’intérieur du moule, sauf le long des bords sur une largeur de 100mm, où il faut en mettre un peu moins.

10. Huiler la plaque supérieure du moule et placer la face huilée vers le bas sur les deux parties inférieures du moule.

11. Fermer les plaques de pressage en utilisant le cric jusqu’à la compression totale du ressort.

12. Attendre que le plastique fonde. 2 minutes avant la fin de la durée optimale de fusion, actionner le cric pour fermer un peu plus les plaques jusqu’à sentir une forte résistance. Voir la Datasheet (Annexe A) pour les températures et durées optimales de fusion.

13. Une fois la durée optimale de fusion atteinte, éteindre la machine. 

14. Il est possible de sortir le moule et de le lester ou de le presser avec des serre-joints pour lui permettre de refroidir à l’extérieur de la machine.

 
## Utilisation du système complet de Presses

1. Vérifier que la machine est éteinte, et que le bouton d’arrêt d’urgence est désenclenché avant de la brancher. Allumer ensuite l’interrupteur principal.

2. Vérifier la température requise pour fondre le type de plastique à utiliser, et régler le contrôleur PID en adéquation.

3. Fermer les plaques de pressage à l’aide du cric et attendre qu’elles chauffent.

4. Pendant le temps d’attente, placer la plaque inférieure du moule sur la table de préparation et appliquer une couche d’huile de silicone. Veiller à retirer tout résidu de la plaque précédente.

5. Placer le cadre du moule au centre de la plaque et appliquer une couche d’huile de silicone sur le haut du moule.

6. Peser la quantité de plastique nécessaire et charger les copeaux dans le moule. Voir la Datasheet (Annexe A) pour les températures et durées de référence. Voir aussi l’affiche à propos des Exemples de Couleur de Plaques (Annexe B).

7. Étaler le plastique équitablement sur l’ensemble de la surface à l’intérieur du moule, sauf le long des bords sur une largeur de 100mm, où il faut en mettre un peu moins.

8. Huiler la plaque supérieure du moule et placer la face huilée vers le bas sur les deux parties inférieures du moule.

9. Quand la machine a atteint la température désirée, ouvrir les plaques en relâchant la pression du cric.

10. Positionner la table de préparation à côté de la face ouverte de la Presse à Plaques.

11. En utilisant l’outil de poussage, transférer le moule de la table à la machine.

12. Fermer les plaques de pressage en utilisant le cric jusqu’à la compression totale du ressort.

13. En attendant que le plastique fonde, préparer le moule pour une nouvelle plaque sur la table de préparation. (Étapes 4-8).

14. 2 minutes avant la fin de la durée optimale de fusion, actionner le cric pour fermer un peu plus les plaques jusqu’à sentir une forte résistance. Voir la Datasheet (Annexe A) pour les températures et durées optimales de fusion.

15. Une fois la durée optimale de fusion atteinte, ouvrir les plaques de pressage en relâchant la pression du cric.

16. En utilisant l’outil de poussage, transférer le moule de la Presse à Plaques vers la Presse de refroidissement.

17. Fermer les plaques de pressage de la Presse à refroidissement en actionnant le cric.

18. Charger et presser la plaque suivante dans la Presse à Plaques (étapes 10-13), puis l’empiler sur la Presse à refroidissement.

19. Répéter les étapes 13 à 20 jusqu’à la fin de votre cycle de travail. Fini? Éteindre et fermer la Presse à plaques.

20. Nettoyer et appliquer une couche d’huile de silicone sur les plaques du moule pour les protéger de l’humidité entre deux sessions.


# ⏳ Maintenance

## Chaque semaine

**Nettoyer les plaques de chauffe**
Utiliser une spatule pour retirer tout résidu de plastique des plaques de chauffe. Vous pourriez être amené à utiliser un alcool à haut degré s’il y a une forte corrosion.

**Graisser les rails**
Utiliser une spatule en bois pour appliquer une généreuse couche de graisse sur tous les rails, nous avons utilisé de la graisse pour moteurs.

## Chaque mois
**Inspection et remplacement des plaques du moule
Inspectez les plaques de vos moules, et remplacez celles arrivées en fin de vie.

**Graisser le mécanisme à ressort**
Détacher le mécanisme à ressort et utiliser une spatule en bois pour appliquer une généreuse couche de graisse sur les points de contact, de la graisse de moteur dans notre cas.

**Inspecter et remplacer les panneaux de contreplaqué**
Inspecter les panneaux de contreplaqué, ils peuvent avoir besoin d’être remplacés s'ils sont endommagés ou tordus.

**Vérifier le site web pour des nouveautés**
La communauté est continuellement en train de développer des hacks, améliorations et techniques pour ensuite les partager sur Internet. Prenez un temps pour voir ce qui existe afin de vous inspirer ou de vous faciliter la tâche.

## + une fois par an
**Nettoyage intensif des plaques de chauffe**
Utiliser une spatule pour retirer tout résidu de plastique des plaques de chauffe. Vous pourriez être amené à utiliser un alcool à haut degré s’il y a une forte corrosion. Une ponceuse rotative peut être utilisée pour enlever les résidus tenaces mais soyez doux et utilisez un grain fin (> 220) afin de préserver la surface de la plaque.

**Vérifier l’huile du cric**
Vidanger et remplacer l’huile des crics.

**Vérifier l’électronique**
Vérifier que la somme des résistances des éléments chauffants sur chacune des trois phases correspond au nombre d’éléments branché sur chaque. Si ce n’est pas le cas, un des éléments chauffants peut être endommagé, vérifiable à l’aide d’une caméra thermique. Inspecter visuellement tous les composants à l’intérieur du boîtier pour repérer d’éventuelles traces d’usure ou de brûlure et remplacer tout composant présentant l’un de ces signes. Reprendre la peinture aux endroits où elle est écaillée. Vérifier que les panneaux extérieurs et le cadre ne portent pas de trace d’usure, et repeindre tout métal exposé pour le protéger de la corrosion.


**Si vous avez besoin d'aide, avez des questions ou cherchez quelqu'un à qui parler dans votre atelier empreint de solitude... Allez sur le canal [#build](https://discordapp.com/invite/XQDmQVT) sur Discord. Nous y discutons de la technique entourant la construction des machines.**
