---
id: sheetpress
title: Informations sur la Presse à Plaques
sidebar_label: - Vue d'ensemble
---


<style>
:root {
  --highlight: #f29094;
  --hover: #f29094;
}
</style>

![Sheetpress](assets/build/sheetpress.jpg)


# 📓 Informations sur la Presse à Plaques
| Spécification                                 |             |
|--------------------------------------|-----------------------|
| 📓 Type                               | Sheetpress              |
| 💎 Version                            | 1                     |
| 💰 Price new material in NL            | +/- €2.550                |
| 💰 Additional system (Cooling press + table)   | +/- €860                  |
| ⚖️ Weight                             | 450kg (Sheetpress)    |
| 📦 Sheetpress Dimension               | 1620 X 1620 X 1780 mm |
| 🔌 Voltage                            | 400V                  |
| ⚡️ AMP                                | 32A                   |
| ⚡️ Power                              | 15kW                 |
| ♻️ Input Flake Size                   | Large, Medium, Small  |
| Max Running Time                     | 8 hours per day       |
| Max temp                             | 300°C                 |
| Tested Plastics                      | HDPE, LDPE, PP, PS    |
| Using foils?                         | Yes                   |
| Input Between Plates                 | 300mm                 |
| Size of Sheet                        | 1000 x 1000 mm        |
| Range of Sheet Thickness             | 4 - 35mm              |
| Sheets Per Day (12mm)                | 3                    |
| Sheets Per Day with full system (12mm)  | 10                    |


# 🌐 Modèle 3D
<iframe width="500" height="600" src="https://b2b.partcommunity.com/community/partcloud/embedded.html?route=embedded-viewer&name=Hot+Press+v1&model_id=96613&portal=b2b&noAutoload=true&autoRotate=false&hideMenu=true&topColor=%23FFFFFF&bottomColor=%23ffffff&cameraParams=false&varsettransfer=" frameborder="0" id="EmbeddedView-Iframe-96613" allowfullscreen></iframe>



# 🌦 Avantages et inconvénients

### Avantages

Le prix total des éléments requis est assez bas en comparaison avec le prix des machines équivalentes sur le marché, avoisinant les 10 000€ pour des presses à plaques supérieures ou égales à 1m2.

C’est la première machine que nous ayons vu avec un transfert de chaleur par conduction seule. Le type de chauffe habituel est par convection ou par conduction et convection. Cela rend la chauffe plus rapide et économe en énergie comparé aux alternatives du marché.

La machine peut être opérée par une seule personne, la plupart des autres nécessitant deux personnes. Aucun autre système sur le marché ne propose “d’outils de convivialité” (?) ce qui oblige à avoir plusieurs personnes pour les différentes étapes de fabrication de plaque de cette taille.
Avec le système complet, vous pouvez chauffer et presser simultanément les plaques ce qui rend le procédé plus rapide.

Facile à construire et accepte une large diversité de moules. Les moules que nous avons conçus sont plus simples et moins chers à fabriquer que les moules traditionnels.

### Inconvénients

La dimension de plaque est de 1m x 1m. Beaucoup de processus de fabrication sont calibrés pour fonctionner avec du 1220mm x 1220mm et il y a des systèmes de presse à plaques pour ces dimensions et même plus grandes disponibles sur le marché.

La plupart des machines présentes dans les ateliers de taille petite ou moyenne nécessitent 16A mais la Presse à plaques nécessite 32A. Comparée aux autres machines Precious Plastic, la Presse à plaques est bien plus grande. Elle ne passe ni à travers une porte simple ou une porte double comme la plupart des autres machines et nécessite au minimum une porte de garage pour la faire rentrer.

La machine n’est pas équipée de roues et n’est pas déplaçable à la force des bras, il faut un tire-palette ou un fenwick pour la déplacer. Certaines machines de presse à plaque sont dotées de roues, mais nous avons préféré ne pas en inclure afin de ne pas compromettre la stabilité de celle-ci. 

# ♻️ Entrée et sortie

### Entrée en plastique

Nous avons réussi à presser des plaques avec du plastique propre de type HDPE, LDPE, PP et PS. La presse peut utiliser n’importe quel type de copeaux (petits, moyens ou grands). De ce fait, afin de gagner du temps de broyage, nous recommandons d’utiliser de grands copeaux que vous obtiendrez en installant le grand tamis sur le Broyeur Pro. Nous avons également réussi à presser des plaques à l’aide de bouchons de bouteilles non broyés; il en a simplement résulté un temps de fonte accru. Voici un tableau pour vous donner un aperçu des différentes épaisseurs de plaque, ainsi que du temps de chauffe et des températures associés.

 ⚠️ Important : vous ne pouvez pas utiliser de plastique sale car il en résultera des résidus brûlés sur le moule qui seront très difficiles à retirer.

 |  Epaisseur de plaque    | HDPE  <br> 220°    | PP <br>  230°   | PS <br> 240°  |
 |------|----------------|------------|--------|
 | 6MM  | 6.5 KG <br> 40 Min   | 6.5 KG <br> 40 Min     | 6.5 KG <br> 40 Min   |
 | 8MM  | 8.5 KG <br> 45 Min   | 8.5 KG <br> 45 Min     | 8.5 KG <br> 45 Min   |
 | 10MM | 11 KG <br> 50 Min    | 11 KG <br> 50 Min      | 11 KG <br> 50 Min    |
 | 12MM | 12 KG <br> 55 Min    | 12 KG <br> 55 Min      | 12 KG <br> 55 Min    |
 | 20MM | 22 KG <br> 60 Min    | 22 KG <br> 60 Min      | 22 KG <br> 60 Min     |



### Jouer avec les couleurs

C’est ici qu’il y a de la place pour jouer. Vous pouvez mélanger n’importe quelles couleurs tant que ça reste le même type de plastique. Parmi les variables avec lesquelles on peut expérimenter il y a la taille des copeaux utilisés. Des copeaux petits, moyens ou grands n’auront pas le même effet. Aussi, utiliser du plastique transparent ajoute un effet de profondeur très intéressant. Plus d’exemples sur le poster dans le kit à télécharger.

![Sheetpress colors](assets/build/sheetpress-colors.jpg)
### Combien de plaques en même temps?

Nous recommandons de ne faire qu’une seule plaque à la fois. Il est sûrement possible de presser de manière plus efficace en en faisant fondre plusieurs à la fois, mais nous n’avons pas encore trouvé de moyen pour les refroidir de manière homogène. Si vous découvrez des façons d’y parvenir, veuillez penser à le partager avec notre communauté en ligne !

### Taille des plaques

Les plaques de presse en aluminium sont de dimensions 1220x1220mm mais il y a souvent un surplus qui s’évacue du moule, donc nous recommandons de faire des moules de 1040mm afin qu’une fois rétractée à cause du refroidissement, la plaque fasse un peu plus d’un mètre. Les bords peuvent ensuite être rabotés afin d’avoir un carré parfait d’1mx1m. L’épaisseur peut aller de 4 à 35mm.


![sheetpress](assets/build/sheetpress-sheets.jpg)


# 🙌 Liens utiles (en anglais)

* [ Acheter ou vendre des pièces et machines sur notre Bazar](https://bazar.preciousplastic.com)
* [ Trouver un fabricant de machine local sur notre carte](https://community.preciousplastic.com/map)
* [ Consulter nos tutoriels pour améliorations et hacks](https://community.preciousplastic.com/how-to)
* [⭐️Comment installer un atelier dédié à la presse à plaques](spaces/sheetpress.md) - ⭐️ traduit ! 
* [⭐️Pour toutes questions aller sur le salon #build de Discord](https://discordapp.com/invite/XQDmQVT)
* [⭐️Guide pour fabriquer une chaise avec des plaques](https://community.preciousplastic.com/how-to/make-a-chair-with-bent-sheets)
* [Post sur le forum à propos de la fabrication du système de Presse à plaques](https://davehakkens.nl/community/forums/topic/v4-sheet-press-system/)
* [Post sur le forum à propos de la construction d’un grand four](https://davehakkens.nl/community/forums/topic/sheet-press-mould-oven/)
* [Post sur le forum à propos d’une version alternative](https://davehakkens.nl/community/forums/topic/v4-sheet-press-system/)
* [Guide pour courber des plaques](https://community.preciousplastic.com/how-to/bend-plastic-sheets)
* [Guide pour construire une étagère avec des plaques](https://community.preciousplastic.com/how-to/make-a-shelving-system)

**Si vous avez besoin d'aide, avez des questions ou cherchez quelqu'un à qui parler dans votre atelier empreint de solitude... Allez sur le canal [#build](https://discordapp.com/invite/XQDmQVT) sur Discord. Nous y discutons de la technique entourant la construction des machines.**
