---
id: finishing
title: Plastique et finitions
sidebar_label: Finitions
---

<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/JwdlLelQWws" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #ffe084;
  --links: rgb(131, 206, 235);
  --hover: rgb(131, 206, 235);
}
</style>

# Plastique et finitions


<div class="videoChapters">
<div class="videoChaptersMain">

Nous avons vu comment travailler le plastique, comment construire et utiliser les machines, et comment fabriquer des objets. Une partie cruciale de ce processus est la finition de vos produits. Il se peut qu'ils sortent du moule en mauvais état, ou qu'ils ne correspondent pas à ce que vous aviez en tête. Nous passerons donc en revue quelques techniques de découpe, de chauffage et de ponçage afin que vous (et votre client) tiriez le meilleur parti de votre produit. Et enfin, nous vous proposons des tampons Precious Plastic pour que vous puissiez marquer votre article avec le type de plastique.

> Conseil d'expert: des moules plus précis entraîneront moins de finition. Si vous avez un moule fraisé CNC très précis, il n'y aura probablement pas de finition. Si vous fabriquez votre propre moule, veillez à le polir soigneusement pour obtenir une finition lisse.


</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00:00 Introduction
- 00:32 Moulds
- 01:48 Types of Plastics
- 03:34 Knife
- 04:25 Heat Gun
- 04:55 Sanding
- 06:22 Mark your Product
- 07:59 PP Trademark


</div>
</div>

### La première chose à retenir

Nous allons encore une fois insister sur ce point : plus votre moule est précis, moins vous aurez de travail à faire en post-traitement. Si vous devez fabriquer 100 bols, le fait de vous assurer que votre moule est super poli vous fera économiser quelques minutes de ponçage. Si vous multipliez ce chiffre par 100 bols, cela fait beaucoup ! Alors, prenez-en de la graine : consacrez du temps à votre moule. Et si le plastique colle, allez à votre quincaillerie locale et achetez du démoulant, cela devrait vous aider.

![Grey Stool](assets/create/stool_grey.jpg)

# Plastiques souples et plastiques durs

Lorsque vous travaillez avec des plastiques souples, comme le HDPE, le PP, le LDPE, ils auront une belle finition mate, et ne seront jamais brillants. Ils sont plus flexibles et plus difficiles à travailler, mais vous pouvez utiliser quelques techniques pour la finition :

- Pour la finition des bords, utilisez un couteau basique ou un ébavureur pour couper les parties rugueuses.
- Pour la finition des surfaces, utilisez une lame ou une spatule métallique si vous voulez lisser une surface.
- Si vous avez des fibres plastiques et filandreuses, vous pouvez utiliser un pistolet thermique sur la surface. Attention toutefois, cela modifiera la texture de votre produit.

Si vous travaillez avec des plastiques durs comme le PS, vous obtiendrez une surface lisse et brillante, beaucoup plus facile à polir et qui peut être très bien poncée.

- Pour la finition des bords ou des surfaces, vous pouvez poncer à la main ou utiliser une machine, de la taille de grain 450 à 1500.
- Pour le polissage, vous pouvez utiliser du dentifrice (oui !) mais uniquement sur les surfaces blanches/claires. Pour tout ce qui est foncé ou coloré, nous vous conseillons d'utiliser un produit de polissage normal que vous trouverez en quincaillerie (nous utilisons un produit de l'industrie automobile). Le polissage de cette manière demande beaucoup de travail, mais le résultat est magnifique !

# Marquage de vos produits

C'est parti ! Bon, maintenant que vous avez une pile de vos produits, il est temps de les marquer. Il est très important de s'assurer qu'une fois que votre produit a atteint la fin de sa durée de vie (espérons que c'est dans longtemps), vous, ou la personne suivante, sache de quel type de plastique il s'agit afin de pouvoir le déchiqueter et le remodeler à nouveau. 
En outre, travailler avec le processus Precious Plastic demande beaucoup de travail : il faut collecter le plastique, le séparer, le déchiqueter, le nettoyer, le mouler, le finir et le marquer. Cette marque peut donc être utilisée non seulement pour indiquer le type de plastique, mais aussi comme une sorte de marque pour les produits en plastique de plus grande valeur. Vous pouvez les acheter sur le bazar de la communauté.

![PP Mark](assets/create/pp_mark.jpg)

# 🙌 Liens Utiles (en anglais) 

- [Différentes textures d'extrusion](https://community.preciousplastic.com/how-to/extrude-different-textures)<br>
- [Une astuce simple pour marquer vos produits](https://community.preciousplastic.com/how-to/make-a-simple-stamp-from-copper-wire)<br>
- [Faites briller vos plaques](https://community.preciousplastic.com/how-to/make-your-sheet-shiny)<br>
- [La transparence du polystyrène](https://davehakkens.nl/community/forums/topic/polystyrene-transparency/)<br>
- [Valuable marble from plastic](https://davehakkens.nl/community/forums/topic/valuable-marble-from-plastic/)<br>

# 🌎 Quelques produits de la communauté

<div class="j-slideshow">

![Community Shredder](../../assets/create/product1.jpg)

![Community Shredder](../../assets/create/product2.jpg)

![Community Shredder](../../assets/create/product5.jpg)

![Community Shredder](../../assets/create/product3.jpg)

![Community Shredder](../../assets/create/product6.jpg)

![Community Shredder](../../assets/create/product4.jpg)

![Community Shredder](../../assets/create/product7.jpg)

</div>

<b>Vous souhaitez faire part de vos commentaires, discuter des produits et de leur conception ou en apprendre davantage sur la communauté ? Rendez-vous sur le canal[#Create](https://discordapp.com/invite/yhmfzTZ) sur Discord. Nous y parlons de la conception des produits et de la fabrication des moules.</b>
