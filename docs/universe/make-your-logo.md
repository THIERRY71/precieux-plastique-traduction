---
id: yourlogo
title: Créer votre propre logo
sidebar_label: Créer votre logo
---

<style>
:root {
  --highlight: #f090b3;
  --links: #f090b3;
  --hover: #f2a5c1;
}
</style>

# Créer votre propre logo!

Salutations, ca me fait plaisir de vous revoir. Nous vous montrerons sur cette page comme faire votre propre logo Precious Plastic. Nous avons quelques lignes directrices et conseils pratiques qui vous seront utiles dans la création de votre logo. Nous fournissons quelques fichiers dans le [kit à télécharger](../download) et avons un outil en ligne super cool mais nous y reviendrons plus tard. L’idée est de vous aider à créer un logo à fort impact pour votre Espace.  

Un logo qui s'appuie sur l'image de marque mondiale de Precious Plastic, personnalisé pour votre situation et qui fonctionne dans l'[Univers](../universe/universe) de la communauté.

> Nous sommes open-source donc vous pouvez faire ce que vous voulez, mais si nous nous tenons tous à ces lignes directrices, tout sera plus fluide et nous pourrons collaborer plus facilement à l’ échelle mondiale.



# Étape 1 - Choisir la bonne couleur

Les couleurs du logo sont utilisées pour identifier la fonction de votre Espace dans l’Univers. Ce code est très pratique :la communauté peut facilement savoir quel rôle vous avez. Faites attention à bien choisir la couleur adaptée à votre Espace.
<center>
| Atelier  |   Espace Communautaire  | Point de collecte | Fabricant de Machine |
|----------|-------------|------------|------------|
| <img src="../../assets/universe/logo-workspace.png" width="150"/> | <img src="../../assets/universe/logo-community.png" width="150"/> | <img src="../../assets/universe/logo-collection.png" width="150"/>   | <img src="../../assets/universe/logo-machineshop.png" width="150"/> |
</center>

# Étape 2 - Choisir le bon nom

Quelques notions à garder en tête lorsque vous choisirez le nom de votre Espace. N’utilisez pas le nom de votre pays, région, département ou ville sauf si vous êtes un Point Communautaire. Nous avons besoin de nombreux Espaces Precious Plastic localement donc si vous utilisez un tel nom, vous empêchez d’autres d’en faire autant. Les noms de lieu se doivent d’être utilisés par les Points Communautaires uniquement. Ils sont LE lieu où aller pour les nouveaux membres. Tout autre nom peut être utilisé. En général, nous recommandons d’utiliser un nom court, qui s'adapte mieux et est facile à retenir.

<img src="../../assets/universe/logo-name.jpg"/>



# Étape 3 - Personnalisation
C’est ici que la créativité s’exprime ! Le drapeau est un symbole très puissant, il est de plus en plus reconnu à travers le monde. Un puissant écosystème est en création. Nous vous recommandons donc de ne pas trop le modifier. Voici quelques exemples de bonnes et mauvaises personnalisations de logo. D’un point de vue général, le conseil principal est de ne pas trop modifier le logo d’origine afin de bénéficier de la notoriété de Precious Plastic à travers le monde.
<img src="../../assets/universe/logo-good-bad.jpg" />

## Ne changez qu’une seule variable
Nous conseillons de ne pas trop modifier le logo, mais ceci est un peu .. abstrait. Le point le plus important auquel se tenir est de **ne modifier qu’une seule variable**. Vous pourriez changer la couleur du drapeau pour l’adapter à votre localisation, changer la couleur de fond ou ajouter votre propre illustration à l’intérieur. Mais ne changez pas plus d’un élément, sinon ça fait désordre.
<img src="../../assets/universe/logo-variables.jpg" />
<img src="../../assets/universe/not-3-variables.jpg" />

## Bien et moins bien
Vous trouverez ci-dessous quelques exemples qui suivent bien nos conseils et d’autres pas. Bonne chance avec votre logo. Si vous avez besoin d’aide ou de retour sur votre travail, rejoignez notre [Discord](https://discordapp.com/invite/zmf98dw).  
<img src="../../assets/universe/logo-examples.jpg" />


# Créons ensemble votre logo !
Nous fournissons 2 options pour que vous fassiez votre logo. Pour chacune d’entre elles, il vous faudra installer les polices Precious Plastic. Prenez soin de bien garder nos indications en tête et si vous avez besoin d’aide, vous pouvez demander sur le canal [#Universe](https://discordapp.com/invite/QUw8A3w)de notre Discord.

1.**À l’ancienne**: Téléchargez tous les fichiers ainsi que les templates du [kit à télécharger](../download). Libre à vous d’utiliser le logiciel de votre choix pour les modifier. Photoshop, Illustrator, GIMP, Inkscape, Sketch, paint... Ce qui vous convient le mieux.

2. **Avec nos nouveaux outils**: Utilisez Figma, un outil gratuit en ligne. Chez Precious Plastic, nous aimons l’utiliser pour créer nos supports graphiques. Il est gratuit et multiplateforme (Windows, Mac et même Linux) ce qui le rend super pratique pour travailler en collaboration.
 
  2.1 Il vous faudra créer votre compte. Allez sur www.figma.com et cliquez sur “Sign” (dans le coin en haut à droite). Vous avez le choix de télécharger l’application ou d’utiliser la version disponible sur leur site. Si vous choisissez la seconde option, il vous faudra installer le “Font Installer” afin de pouvoir utiliser les polices Precious Plastic dans l’outil. 

  2.2 Si vous souhaitez en apprendre plus sur Figma et l’utilisation de l’éditeur, vous pouvez visionner [cette vidéo](https://youtu.be/DSrbwCrEIII).
	
  2.3 Une fois que c’est fait, nous sommes fins prêts à créer votre logo. Suivez ce lien : https://www.figma.com/file/YpkQ9zzWMLS1R47pJcKBxJ/Logo-Generator

Toutes les instructions sont incluses dans le fichier. **Amusez-vous bien et soyez créatif**!
  
<img src="../../assets/universe/figma-logo.gif" />

<b>Vous avez des questions ou avez besoin d’aide pour créer votre logo ? Allez sur le salon [#Universe](https://discordapp.com/invite/QUw8A3w) de notre Discord. On y discute de votre place dans l’Univers, des rôles, de l’image de marque et de votre logo. </b>
