---
id: compression
title: Construire le four à compression
sidebar_label: Four à compression
---

<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/ogI8kt0w43Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #f29094;
  --hover: #f29094;

}
</style>
<div class="videoChapters">
<div class="videoChaptersMain">

# Construire le four à compression

### Quelle est cette machine?

La machine à compression est essentiellement un four de cuisine électrique pour chauffer le plastique associé à un mécanisme de compression (un cric de voiture) pour mettre votre moule sous pression. Le processus est plus lent que les autres machines Precious Plastic, mais il permet d'utiliser des moules plus grands. Il peut être utilisé pour créer des matières premières, mais aussi des plaques ou des formes qui peuvent être travaillées pour fabriquer de nouveaux produits avec un aspect floconneux spécifique au plastique (qui est incroyable !)


> Conseil d’expert: La machine à compression est parfaite pour l’expérimentation et faire des essais. Mais il est fortement conseillé d’utiliser une extrudeuse ou une machine à injection pour une production régulière.


</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00:00 Introduction
- 00:39 Make the oven
- 03:51 Framework
- 04:55 Press system
- 08:02 Electronic
- 11:21 How it works

</div>
</div>

# 📓 Informations techniques

📓 Type | Compression Machine
--- | ---
💎 Version | 2.0
💰 Prix des matériaux neufs aux Pays Bas | +/- €350
💰 Prix des matériaux de récup' aux Pays Bas | +/- €150
⚖️ Poids | 30 kg
📦 Dimensions | 500 x  590 x 1590 mm
⚙️ Compression | 2t car jack
🔌 Voltage | 220V
⚡️ AMP | 2.6A
♻️ Taille des paillettes utilisables  | Petites ou Moyennes  |


# 🌐 Modèle 3D
<iframe width="500" height="500" src="https://b2b.partcommunity.com/community/partcloud/embedded.html?route=embedded-viewer&name=Compression+Basic+V2.0&model_id=96647&portal=b2b&noAutoload=false&autoRotate=false&hideMenu=true&topColor=%23dde7ed&bottomColor=%23ffffff&cameraParams=false&varsettransfer=" frameborder="0" id="EmbeddedView-Iframe-96647" allowfullscreen></iframe>




# 🛠 Machines et compétences requises
Four de compression | Outils nécessaires | Compétences requises
--- | ---| ---
<img style="margin-left: 0;" src="../../assets/build/thumb-compression.jpg" width="100"/>  | - Perceuse à colonne <br> - Poste à souder <br> - Meuleuse d'angles | - Soudure (intermédiaire) <br> - Assemblage (intermédiaire) <br> - Electronique (intermédiaire)

# ⚡️ Boîtier électronique
Explication des composants électriques à l'intérieur de cette machine. Plus d'informations et de schémas sont à retrouver dans le kit en téléchargement.

*  **Contrôleur PID**: le cerveau de la machine qui sert à régler les températures souhaitées. Il enverra de l'énergie aux éléments de chauffe jusqu'à ce que le PV (point variable) corresponde à la SV (valeur définie). Pour ce faire, il utilise les lectures du thermocouple et du SSR.
* **SSR**: le Solid State Relay est un interrupteur électronique qui s'ouvre ou se ferme en fonction du signal qu'il reçoit (du PID).
* **Thermocouple**: fondamentalement une sonde de température.
* **Élément chauffant**: élément chauffant qui s'adapte autour d'un tuyau.
* **Interrupteur d'alimentation**: interrupteur mécanique.
* **Indicateur lumineux**: LED s'allumant lorsque la machine est sous tension (souvent située sur l'interrupteur d'alimentation).
* **Câble d'alimentation**: câble d'alimentation domestique courant.

> Conseil d'expert: Voici un [bon forum à propos de l'électronique (en anglais)](https://davehakkens.nl/community/forums/topic/the-big-electronics-topic/)

# 🛠 Trucs et astuces lors de la fabrication

- Chercher des fours d'occasion ou de récup.
- Essayer de faire le trou dans le four pas plus grand que nécessaire. Cela permettra de réduire l'isolation nécessaire.
- Jeter un œil à la mise à jour Compression v2.1 avant de plonger dans ces plans.

# ♻️ Entrée et sortie


<b>Type:</b> HDPE, LDPE, PP, PS<br>
**Sortie:** 1 pièce toutes les 40 minutes. Dépend largement du moule.


# ⚙️ Fonctionnement et maintenance

La machine à compression est simple à comprendre et très puissante si elle est correctement maîtrisée. Ce procédé peut être utilisé par une seule personne la plupart du temps. Encore une fois, le processus de création est très lié aux moules et à la fabrication de moules. Si vous avez un moule précis, vous pouvez créer de beaux produits. Le moule que nous fournissons est très basique afin que tout le monde puisse comprendre le processus, mais nous vous encourageons fortement, ainsi que votre équipe, à envisager la fabrication de nouveaux moules pouvant être utiles dans votre zone.

### Comment faire fonctionner la compression              

### Commencer

1. Allumer le four et régler la température souhaitée.
2. Attendre 20 minutes pour que la température souhaitée soit atteinte.

### Production

1. Peser la quantité de matière nécessaire pour le moule, et ajouter 20%.
2. Remplir le moule de plastique.
3. Mettre la partie supérieure du moule sur le plastique.
4. Mettre le moule au four.
5. Laisser chauffer pendant 15 minutes.
6. Retourner le moule à 180 ° dans le four.
7. Laisser chauffer encore 15 minutes.
8. Compresser le moule.
9. Sortir le moule du four.
10. Mettre des serre-joints pour maintenir la pression.
11. Placer un autre moule préparé dans le four.

### Refroidir
1. Nettoyer l'intérieur du four de tout plastique fondu.

### Trucs et astuces lors de l'utilisation

1. Il est conseillé de chauffer d'abord le plastique jusqu'à ce qu'il soit fondu, puis d'appliquer la pression.
2. Ne vous précipitez pas, assurez-vous que le plastique soit complètement fondu dans le moule.

# 🔓 Dépannage
- **Le plastique déborde d'un côté du moule.** Ceci est souvent lié à un désalignement entre le moule, et la plaque de pressage ou le four lui-même. Assurez-vous que tout soit parallèle au sol autant que possible.
- **Le produit reste collé au moule.** Essayez de chauffer doucement le moule pour faciliter la libération. Appliquez une couche de produit de démoulage sur le moule avant de le charger.

# 🌦 Avantages et inconvénients
 Avantages | Inconvénients
--- | ---
Facile à fabriquer 	     | Le processus est lent
Réutilisation de fours d’occasion	 | Taille du moule limitée au four
Fonctionne sur 220V | Peu économe en énergie
Des produits plus gros que l'injection   |
Des motifs uniques |  

# 🌎 Construit par la communauté

<div class="j-slideshow">

![Community Compression](assets/Build/community/compression3.jpg)

![Community Compression](assets/Build/community/compression1.jpg)

![Community Compression](assets/Build/community/compression2.jpg)

</div>


# 🙌 Liens utiles (en anglais)
- [⭐️Upgrade: Compression V2.1](https://community.preciousplastic.com/how-to/compression-machine-21)
- [Hack: Compression](https://davehakkens.nl/community/forums/topic/machine-development-compression-oven/)
- [Hack: Solar Powered Melting Machine](https://davehakkens.nl/community/forums/topic/solar-powered-melting-machine/)
- [How-to: Wall Clock](https://www.youtube.com/watch?v=e_jqIvSFfI4)<br>

**Si vous avez besoin d'aide, avez des questions ou cherchez quelqu'un à qui parler dans votre atelier empreint de solitude... Allez sur le canal [#build](https://discordapp.com/invite/XQDmQVT) sur Discord. Nous y discutons de la technique entourant la construction des machines.**
