---
id: extrusionprorun
title: Utilisation de l'Extrudeuse Pro
sidebar_label: - l'Utiliser
---

<style>
:root {
  --highlight: #f29094;
  --hover: #f29094;
}
</style>

![Extrudeuse](assets/build/extruderpro-run.jpg)

# Utilisation et Maintenance de l’Extrudeuse Pro
Houra ! Vous vous êtes construit une Extrudeuse Pro ! Il est temps de s’amuser et faire des profilés et des briques. Ci-dessous nous expliquons comment faire, quelles températures utiliser, comment organiser le processus de travail, ainsi que d’autres choses pratiques à prendre en compte telle que la maintenance.

## 🏃‍♀️ Extruder le Plastique

### Mise en fonctionnement

1. Allumer l’interrupteur principal
2. Sélectionner les températures souhaitées sur chacun des 3 PID
3. Verser le plastique dans la trémie
4. Attendre que les sondes thermiques affichent les températures souhaitées (environ 15/20 min)
5. Mettre votre masque / mise en marche de la ventilation
6. Mettre en marche le moteur en vitesse réduite pour vérifier le flux du plastique
7. (Optionnel) S’il y a eu un changement de type de plastique par rapport à l’utilisation précédente, il faut nettoyer le tube de l’ancien type de plastique en laissant le moteur tourner jusqu’à ce que l’ancien type soit remplacé par celui que vous venez de mettre dans la trémie.
8. La machine est prête à produire !

### Production

1. Si un moule est requis pour le produit désiré, le fixer à la buse
2. Le flux peut être ajusté en augmentant ou baissant la vitesse sur le VFD (de 0 à 50/60HZ)
3. Vous pouvez commencer et appuyer sur le bouton démarrer sur le VFD
4. S’assurer qu’il y a toujours de la matière dans la trémie pour réduire la chance de formation de bulles dans le plastique extrudé


# 👌 Conseils et astuces pendant l’utilisation

Avant de fixer le moule, vérifiez au niveau de la buse que le plastique soit bien homogène, fluide et que la couleur corresponde à vos attentes.

Si vous changez de type de plastique laissez la machine tourner pendant une minute pour s’assurer que le tube se nettoie du type de plastique précédent.

Le plastique broyé de dimension inférieure à 7 mm2 alimente l’extrudeuse de façon optimale. Des copeaux plus longs ou plus fins ont tendance à se bloquer dans la trémie. Si les copeaux sont trop gros, vous pouvez les repasser dans la broyeuse ou installer un système pour faire vibrer la trémie. Les granulés de plastique améliorent le flux de plastique dans l’extrudeuse.

S’il n’y a pas de plastique qui sort de l’extrudeuse et que les sondes de température affichent des chiffres anormalement élevés, cela veut dire qu’il y a beaucoup de friction qui se produit dans le tube et que la pression interne est en train de monter. Éteignez immédiatement la machine car il y a sûrement une obstruction quelque part qui empêche le plastique de sortir.

### ⚠️ Faites toujours bien attention et utilisez des gants adaptés lorsque vous manipulez du plastique chaud !

Assurez-vous qu’il y ait toujours assez de plastique dans la trémie. Si vous entendez un son aigu ou un frottement métallique, c’est probablement que votre tube est vide. Dans ce cas, remplissez la trémie ou arrêtez l’extrusion pour éviter d'abîmer la machine.

# ⏳ Entretien de la machine

L’extrudeuse est une machine simple et robuste. Cependant, si vous comptez la garder longtemps, il convient de suivre quelques conseils d’entretien :

### Lubrification des roulements

Assurez-vous qu’il y ait beaucoup de graisse dans les roulements. Vous pouvez ajouter un peu de graisse à l’aide de la buse de graissage tous les mois pour s’assurer qu’ils soient toujours bien lubrifiés.

### Lubrification du réducteur

Selon le type de réducteur utilisé, celui-ci peut-être lubrifié avec de la graisse ou de l’huile. Chaque réducteur est équipé d’une plaque de lubrification. Vérifiez et suivez les instructions d’entretien fournies avec votre moteur et réducteur.

### Entretien du tube

Votre tube est en acier doux, et conçu pour être facile à fabriquer. Vous allez vouloir éviter que de la rouille se forme à l’intérieur du tube. Laisser le plastique refroidir dans le tube empêchera la rouille de se former à l'intérieur de celui-ci. Si vous ne comptez pas utiliser l’extrudeuse pendant un moment, vous pouvez ajouter un peu d’huile de silicone après avoir vidé le tube. La vis est renforcée et est supposée avoir une durée de vie supérieure au tube. Fabriquez un nouveau tube si nécessaire.

### Vérifiez le serrage des vis

Vérifiez fréquemment le couple de serrage des vis (coupleur, adaptateur moteur, etc.) de l'extrudeuse.

# 🔓 Dépannage

### 2 bagues chauffantes cessent de fonctionner en même temps

Si vous remarquez que 2 bagues chauffantes alimentées ensemble cessent de fonctionner, il y a de grandes chances que le problème viennent du SSR (Solid State Relay). Ouvrez la boîte d’électronique et vérifiez que les branchements soient corrects. Si le câblage est bon mais que le LED ne s’allume pas au démarrage du SSR, il y a de fortes chances que vous deviez le remplacer.

Si cela ne fonctionne toujours pas après remplacement du SSR, c’est qu’il faut changer le contrôleur PID.

### Une bague chauffante cesse de fonctionner

Si vous remarquez qu’une bague chauffante cesse de fonctionner, ouvrez le boîtier électronique, vérifiez les câblages et assurez vous que le SSR qui est branché dessus a son LED qui s’allume lorsque vous mettez l’extrudeuse en marche. Si elle s’allume, alors le problème vient de la bague chauffante et il vous faut la remplacer.

Les bagues chauffantes peuvent cesser de fonctionner si vous les tordez trop, faites attention lorsque vous les manipulez pour les mettre ou les retirer du tube. Aussi, ne dépassez pas les températures maximum des bagues chauffantes.

### Surchauffe du moteur

Si vous constatez que votre moteur surchauffe, testez votre moteur dans un environnement chaud ou laissez le tourner pendant un longue période à faible vitesse ( moins de 20 Hz). Si le ventilateur du moteur n’est pas suffisant pour le refroidir, il vous faudra en installer un ventilateur complémentaire au-dessus du moteur.

### La vis d’extrusion ne tourne pas

Une surcharge du moteur est souvent la principale cause d’un dysfonctionnement de celui-ci. Vérifier le message d’erreur indiqué sur le VFD vous guidera sur les cause de ce dysfonctionnement (si vous utilisez un BOSCH REXROTH EFC 5610 , une surcharge du convertisseur sera indiquée par le code OL-1)

Vous pouvez aussi vérifier la conformité de la connectique sur la plaque à bornes, en fonction de votre connexion au secteur, vous devrez connecter votre moteur triphasé en mode triangle ou étoile. Cela influera sur le couple disponible par le moteur. Vérifiez également le couple de serrage du coupleur de vis d'extrusion si l'arbre tourne et non la vis d'extrusion.

Cela peut aussi être causé par le fait que le plastique mis dans la trémie n’ait pas assez fondu et est donc resté sous forme solide, ou que son MIF (Indice de Fusion) est trop bas pour être utilisé dans la machine.

### Corps étranger dans la trémie

Si quelque chose tombe dans la trémie et bloque la vis d’extrusion, il vous faudra essayer de mettre doucement le moteur en marche arrière : vous pouvez inverser la direction du moteur en inversant physiquement les 2 phases du moteur. Vous pouvez également y arriver en changeant les paramètres d’exécution du VFD.

Faites bien attention à ne pas faire tomber des pièces métalliques dans la trémie. Vous pouvez retirer les objets magnétiques présents dans votre plastique broyé en le sondant avec un aimant. Retirez le tube pour enlever les débris si nécessaire.

### Plastique non uniforme en sortie de machine

 Le problème le plus récurrent lors de l’extrusion est d’avoir un plastique non uniforme en sortie de machine. Il y a quelques raisons qui peuvent expliquer ce phénomène:

1. Du plastique sale a peut-être bouché la buse rendant difficile la sortie du plastique propre
2. Deux types de plastiques différents ont été mélangés. Dans ce cas l’un des deux fond alors que l’autre obstrue le flux de plastique au niveau de la buse
3. La température n’est pas assez élevée et le plastique dans le tube ne réussit pas à fondre entièrement
4. Vous pouvez résoudre les problèmes indiqués ci-dessus en vérifiant l’intégrité et la pureté de votre matière ou en augmentant la température. Au fur et à mesure que vous ajustez ces paramètres, pensez à vider le tube entièrement avant de faire un nouveau produit.


**Si vous avez besoin d'aide, avez des questions ou cherchez quelqu'un à qui parler dans votre atelier empreint de solitude... Allez sur le canal [#build](https://discordapp.com/invite/XQDmQVT)sur Discord. Nous y discutons de la technique entourant la construction des machines.**
