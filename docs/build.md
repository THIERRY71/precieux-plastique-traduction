---
id:build
title:Construire
sidebar_label:Intro

---
<div class="videocontainer"> <iframe width="800" height="400" src="https://www.youtube.com/embed/CRM7Jip2swU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </div>

<style> :root { --highlight: #f29094; --hover: #f29094; } </style>

<div class="videoChapters"> <div class="videoChaptersMain">

# CONSTRUIRE LES MACHINES PRECIOUS PLASTIC

### Quelles sont ces machines ?

Il est temps de construire des machines ! Dans cette section, nous vous présenterons toutes les machines que nous avons développées chez Precious Plastic - le broyeur, la presse à injection, le four à compression et l'extrudeuse, originales et axées pour les débutants ainsi que les machines plus robustes que nous avons développées dans la version 4 - la presse à plaques, le broyeur Pro et l’extrudeuse Pro 💪

Nous décrivons comment construire chaque machine et où se procurer les pièces, ainsi que les outils et compétences nécessaires pour être opérationnel. Les vidéos décrivent en détail chaque version et le pack de téléchargement de chaque machine regroupe tout ce dont vous avez besoin pour commencer.

> Conseil : concentrez-vous sur une machine, l’ensemble de compétences et les résultats associés. Maîtrisez cela et devenez un expert dans l'univers Precious Plastic !

</div> <div class="videoChaptersSidebar">

### Video Chapters

* 00:12 Introduction
* 01:35 Shredder
* 01:53 Extruder
* 02:33 Compression
* 02:57 Injection

</div> </div>

![V3 and V4 Machines](assets/Build/v3v4machine.jpg)

## Pourquoi ces machines sont-elles importantes ?

Nous n'avons pas inventé les machines de transformation du plastique. Elles existent déjà dans l'industrie et peuvent créer des produits à une vitesse fulgurante - elles sont presque trop efficaces, ce qui contribue à la quantité de déchets plastiques que nous avons dans le monde. Mais travailler avec du plastique recyclé peut être imprévisible et c'est pourquoi l'industrie ne veut pas travailler avec ce type de matériau - il est plus facile, moins cher et plus fiable d'utiliser du plastique vierge propre. Nous avons donc basé nos machines sur celles de l'industrie mais les avons rendues moins chères, plus faciles à construire et beaucoup plus abordables :

![Industry Vs Precious Plastic](assets/Build/industry_vs_pp.svg)

Cette approche inclusive augmente l'accessibilité et l'adoption dans le monde entier et est développée en utilisant des technologies et des matériaux de base afin que les machines soient facilement comprises, reproduites et réparées par des personnes du monde entier avec des compétences de base dans le travail du métal et en construction. Et depuis les nombreuses années que nous faisons cela avec vous (notre communauté), il est plus facile que jamais de comprendre comment construire ces machines dans votre communauté locale et obtenir le soutien des constructeurs et fournisseurs locaux. Et nous adorons vous voir les détourner et les améliorer !

> Conseil : utilisez la carte pour voir quelles machines se trouvent proches de chez vous !

### Nos machines

Le principe sous-jacent derrière les machines est le même pour tous (sauf le broyeur). Chaque machine applique de la chaleur au plastique, qui se retrouve fondu, pour être ensuite pressé dans un moule ou une forme et est finalement refroidi pour retrouver son état solide. C’est toute la magie du recyclage. Simple comme bonjour, non? Chaque machine est unique avec ses propres workflows, résultats, précautions, comportements et problèmes, mais le concept fondamental reste le même. Plus vous travaillerez avec elles, plus vous les comprendrez et améliorerez le recyclage. Alors allons-y !

| Machines | Description |
|----------|-------------|
| <img src="../assets/build/thumb-shredder.jpg" width="150"/> | __Broyeur__ <br> Notre broyeur coupe le plastique en petits flocons qui peuvent être utilisés par les autres machines pour être fondus. Privilégiez le Broyeur Pro pour des cadences plus élevées. |
| <img src="../assets/build/thumb-extrusion.jpg" width="150"/> | __Extrudeuse__ <br> Le plastique broyé entre, fond et se transforme en un filament de plastique. Vous pouvez laisser cours à votre créativité et l'enrouler autour d'un gabarit ou remplir des moules. Privilégiez la version Pro pour des cadences plus élevées. |
| <img src="../assets/build/thumb-injection.jpg" width="150"/> | __Presse à injection__ <br> Machine simple à construire et à utiliser. Le plastique fond dans le tube et est pressé dans un moule. Vous pouvez créer des objets de base mais aussi très détaillés. Parfait pour l'éducation et les petites productions. |
| <img src="../assets/build/thumb-compression.jpg" width="150"/> | __Four à Compression__ <br> Un four qui peut chauffer votre plastique dans un moule et le comprimer. Convient bien à l’expérimentation et l’apprentissage mais pas très adapté pour la production. |
| <img src="../assets/build/thumb-sheetpress.jpg" width="150"/> | __Presse à plaques__ <br> Une imposante machine qui peut faire de grandes plaques. Elle peut traiter un large volume de plastique par jour. C'est un processus relativement nouveau, donc il y a encore beaucoup de choses à apprendre et à comprendre :) |
| <img src="../assets/build/thumb-extrusion-pro.jpg" width="150"/> | __Extrudeuse Pro__ <br> Une version plus robuste de notre petite extrudeuse. Conçue pour fonctionner pendant de longues heures et transformer davantage de plastique. On peut compter sur cette machine pour les grandes productions. |
| <img src="../assets/build/thumb-shredder-pro.jpg" width="150"/> | __Broyeur Pro__ <br> Version améliorée de notre petit broyeur, la principale différence étant l’ajout d’un deuxième essieu. Parfait pour déchiqueter rapidement le plastique en petits flocons. Mais moins performant pour la production de granulés. |

**Si vous avez besoin d'aide, avez des questions ou cherchez quelqu'un à qui parler dans votre atelier empreint de solitude... Allez sur le canal** [**\#build**](https://discordapp.com/invite/XQDmQVT) **sur Discord. Nous y discutons de la technique entourant la construction des machines.**
