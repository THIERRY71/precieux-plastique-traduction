---
id: basics
title: Les Fondamentaux du Plastique
sidebar_label: Les Fondamentaux du Plastique
---
<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/fsqLJNyrVss" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #ffe084;
  --links: #29bbe3;
  --hover: rgb(131, 206, 235);
}
</style>

# Les Fondamentaux du Plastique

<div class="videoChapters">
<div class="videoChaptersMain">

### Commençons par le commencement.

**Qu'est-ce que le plastique? D’où provient-il?** Combien en utilisons-nous et qu'est-ce qui est recyclé ? Quels sont les différents types de plastique et comment les reconnaître ? Cette partie traitera des bases du plastique pour se familiariser rapidement avec le matériau que nous allons travailler !

> __Conseil d'expert__: regardez sous un produit en plastique et voyez s’il comporte un petit triangle entourant un chiffre. Ce chiffre indique la catégorie du plastique, cela aide à retrouver son type.

</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 0:11 Introduction
- 00:18 Qu'est-ce que le plastique?
- 01:15 Différents types
- 02:45 Reconnaître les plastiques
- 05:18 Transformer le plastique


</div>
</div>

# Qu'est-ce que le plastique?

Le plastique est partout autour de nous.

![Plastic Pile](assets/plastic/plasticpile.svg)

Le mot plastique fait partie du vocabulaire courant depuis longtemps, mais que signifie-t-il réellement? Le mot est dérivé du grec “plastikos” qui signifie “capable d'être formé ou moulé”, faisant référence à la  malléabilité du matériau lors de sa transformation. Cette propriété permet au plastique d'être coulé, pressé ou extrudé pour obtenir des formes variées - des films, des fibres, des plaques, des tubes, des bouteilles, etc...

Les plastiques sont des produits chimiques de synthèse extraits principalement du pétrole et fabriqués à partir d'hydrocarbures (chaînes composées d'atomes d'hydrogène et de carbone). La plupart des plastiques sont des polymères, de longues molécules composées de nombreuses répétitions d'une molécule de base appelée monomère. C’est cette structure particulière qui rend le plastique particulièrement durable et résistant dans le temps.

En raison de leur coût relativement faible, de leur facilité de fabrication et de leur polyvalence, les plastiques sont utilisés dans une part énorme et croissante de produits : des bouteilles de shampooing aux fusées spatiales. L'omniprésence et le volume même de la production de plastique (on en trouve partout!) causent de graves dommages à l'environnement en raison de sa lente décomposition (des études récentes disent 500 ans)  garantie par les liaisons fortes entre ses molécules. 

Pensez-y de cette façon, tous les plastiques utilisés par vos parents, vos grands-parents et vos arrière-grands-parents sont toujours là et pollueront la planète pendant encore quatre siècles.

La plupart des plastiques sont mélangés à d'autres composés organiques ou inorganiques, appelés additifs, dont le rôle est d’améliorer les performances ou de réduire les coûts de production. La quantité d'additifs varie largement en fonction de l'application et du type de plastique.

Donc vous pouvez en trouver partout dans le monde, et il se retrouve dans des endroits où nous ne voudrions certainement pas qu'il soit...

![PP Image](assets/plastic/beachplastic.jpg)

### Nous produisons plus de 300 millions de tonnes métriques de plastique neuf chaque année 🤯

Ce qui n'est pas très intelligent, surtout quand nous avons tellement de matière déjà existante que nous pourrions utiliser. Le plastique vierge est fabriqué à partir de pétrole, un précieux combustible fossile dont nous manquons ; il est utilisé pour fabriquer des produits bon marché qui sont destinés à être jetés après une très courte période d'utilisation. Ce n'est pas très malin. Et comme moins de 10% du plastique produit est effectivement recyclé, le reste finit dans les décharges, dans l'océan ou est brûlé. Hein?

Alors, comment pouvons-nous résoudre ce problème? Eh bien, il est temps de recycler!


> __Conseil d'expert__: en parallèle de vouloir le recycler, il est important que chacun cherche à réduire son utilisation de plastique neuf.

# Différents types de plastique

Premièrement, il existe deux grandes catégories de plastique : les thermoplastiques et les thermodurcissables.

![Thermoplastic - Thermoset](assets/plastic/thermoplasticthermoset.svg)

### Thermodurcissables
Les plastiques thermodurcissables contiennent des polymères qui se réticulent et créent une liaison irréversible, ce qui signifie qu'ils ne peuvent pas être refondus - une fois leur forme prise, ils seront solidifiés pour toujours. Considérez les plastiques thermodurcissables comme du pain: une fois que le pain est fait, si vous essayez de le chauffer, il brûle. Aucun de ces plastiques ne peut être recyclé. Voici quelques exemples de plastiques thermodurcissables:

![Thermoset Examples](assets/plastic/thermosetex.svg)

### Thermoplastiques
Les thermoplastiques sont des polymères plastiques qui deviennent mous lorsqu'ils sont chauffés et durs une fois refroidis. Les matériaux thermoplastiques peuvent être refroidis et chauffés plusieurs fois: lorsqu'ils sont chauffés, ils fondent et redeviennent durs lorsqu'ils refroidissent. Considérez les thermoplastiques comme du beurre: il peut être chauffé et refroidi plusieurs fois, il fond et se fige à nouveau. Exemples de thermoplastiques:

![Thermoset Examples](assets/plastic/thermoplasticex.svg)

Heureusement, 80% des plastiques dans le monde sont des thermoplastiques (🎉), ce qui signifie qu'ils peuvent être recyclés et transformés. Les thermoplastiques (que nous appellerons simplement plastique) sont divisés en sous-catégories supplémentaires en fonction de leur structure et de leurs propriétés, et peuvent être reconnus par leur nom ou numéro qui doit généralement être imprimé ou estampé quelque part sur vos objets.

Les plus courants étant:

<img style="margin-left: 0; margin-top: 50px" src="../../assets/plastic/plastictypesoverview.svg" width="80%"/>

<img style="margin-left: 0; margin-top: 50px" src="../../assets/plastic/type-pet.svg" width="80"/>

### PET (1): Polytéréphtalate d'éthylène
Le PET est un plastique très solide qui peut être facilement reconnu pour son aspect transparent - toutes les bouteilles d'eau et de soda sont fabriquées à partir de PET ainsi que certains pots, peignes, sacs, fourre-tout, tapis et cordes, et sont plus couramment recyclés. Récemment, le PET a été recyclé en fils et transformé en vêtements. Ce plastique est un peu plus complexe à travailler, nous vous conseillons de commencer par d'autres plastiques.

<b>Propriétés:</b> léger, résistant aux chocs, rigide / semi-rigide<br>
<b>Avantages:</b> solide et rigide, résistant à l'eau et à l’oxydation, bonnes propriétés électriques<br>
<b>Inconvénients:</b> rétrécissement important au refroidissement, dégradation thermique, fumées nocives<br>
<b>Sécurité:</b> Moyenne<br>
<b>Avertissement ⚠️</b> fumées nocives pendant la transformation, certaines études suggèrent que des microparticules nocives s’en détachent lors d'une utilisation à long terme<br>
<b>Utilisations courantes:</b> bouteilles moulées par soufflage (bouteilles d'eau, de soda, de jus), emballage, film,  gaines électriques<br>
<b>Meilleures façons d'utiliser avec des machines PP:</b> on y travaille!

<img style="margin-left: 0; margin-top: 50px" src="../../assets/plastic/type-hdpe.svg" width="80"/>

### PEHD (2): Polyéthylène haute densité
Le PEHD est souvent utilisé pour les contenants alimentaires et de boissons, ainsi que pour les bouteilles de lait, d’huile de moteur, de shampoing, de gel douche, les détergents, les javellisants, les jouets et les bouchons de bouteilles. Les produits de ce type de plastique sont souvent plus faciles à collecter, trier et nettoyer. Le PEHD fonctionne très bien avec les machines Precious Plastic, ce qui le rend idéal pour commencer!

<b>Propriétés:</b> inerte, thermiquement stable, robuste et forte résistance à la traction<br>
<b>Avantages:</b> bon marché, haute résistance chimique, propriétés électriques, texture cireuse, réagit bien au frottement<br>
<b>Inconvénients:</b> moins rigide que le PP, brûle facilement, mauvaise résistance aux UV, rétrécissement important au refroidissement<br>
<b>Sécurité:</b> Bonne<br>
<b>Avertissement ⚠️</b> Le PEHD lui-même (lorsqu'il ne brûle pas) n'est pas dangereux à utiliser, mais les additifs peuvent l’être. Il n'est pas possible de voir quel type d'additifs ont été utilisés dans la composition.<br>
<b>Utilisations courantes:</b> tuyaux, jouets, bols, caisses, film d'emballage<br>
<b>Meilleures façons d'utiliser avec les machines PP:</b> le PEHD fonctionne de manière très similaire au PP, faible température de fusion et facilité de moulage. Un excellent matériau à utiliser!

<img style="margin-left: 0; margin-top: 50px" src="../../assets/plastic/type-pvc.svg" width="80"/>

### PVC (3): Chlorure de polyvinyle
<b>Le PVC est toxique donc nous ne travaillons pas avec.</b> Il se trouve le plus souvent dans les tuyaux de plomberie et libère du chlorure lorsqu'il est chauffé - son utilisation n’est pas recommandée avec les machines Precious Plastic!

<b>Propriétés:</b> isolant, chimiquement inerte<br>
<b>Avantages:</b> bon marché, résistance aux acides et alcalis, ignifuge, rigide et solide<br>
<b>Inconvénients:</b> la surchauffe entraîne sa dégradation, cassant en dessous de 0 ° C, décolore sous une forte lumière UV, une haute densité pour le thermoplastique, HCL et dioxines lorsqu'il est brûlé<br>
<b>Sécurité:</b> Dangereux - ne pas utiliser<br>
<b>Avertissement ⚠️</b> charges dangereuses et libération de HCL et dioxines lors de la dégradation ou de la combustion<br>
<b>Utilisations courantes:</b> Flexible: simili-cuir, joints, cache-câbles, ruban adhésif. Rigide: tuyaux, produits de construction, bottes, film, semelles, gaine thermorétractable<br>
<b>Meilleures façons d'utiliser avec les machines PP:</b> ne l'utilisez pas!

<img style="margin-left: 0; margin-top: 50px" src="../../assets/plastic/type-ldpe.svg" width="80"/>

### LDPE (4): Polyéthylène basse densité
Le LDPE est largement utilisé pour les emballages en plastique, les sacs à sandwich, les bouteilles compressibles et les sacs d'épicerie en plastique. Habituellement, le LDPE n'est pas fréquemment recyclé, car il n'est souvent pas estampillé, est trop léger et a tendance à être plus difficile à nettoyer, mais fonctionne plutôt bien avec les techniques de Precious Plastic. Une technique de recyclage populaire pour les sacs en plastique consiste à les repasser pour en faire une sorte de textile plus durable.

<b>Propriétés:</b> chimiquement inerte, flexible, isolant<br>
<b>Avantages:</b> bon marché, résistance chimique et à l'hydrolyse, haute résistance aux chocs (à basse température), bonne capacité de transformation<br>
<b>Inconvénients:</b> faible résistance à la traction, faible rigidité, basse température maximale, brûle facilement, mauvaise résistance aux UV, rétrécissement important au refroidissement<br>
<b>Sécurité:</b> Bonne<br>
<b>Avertissement ⚠️</b> Le LDPE lui-même (lorsqu'il ne brûle pas) n'est pas dangereux à utiliser, mais les additifs peuvent l’être. Il n'est pas possible de voir quel type d'additifs ont été utilisés dans la composition<br>
<b>Utilisations courantes:</b> bols, couvercles, jouets, conteneurs, films, bouteilles compressibles, tuyaux, sacs, draps<br>
<b>Meilleures façons d'utiliser avec les machines PP:</b> le LDPE se trouve souvent sous forme de feuille et n'est pas idéal à broyer. Mais il est facile à repasser avec des mélanges de feuilles/films pour imiter un effet [métallique](https://www.youtube.com/watch?v=Pp4vmfVlm2k) ou [marbré.](https://www.youtube.com/watch?v=LV6Zp9i0mOc)

<img style="margin-left: 0; margin-top: 50px" src="../../assets/plastic/type-pp.svg" width="80"/>

### PP (5): Polypropylène
Le PP est l'un des plastiques les plus couramment disponibles sur le marché, il est solide et peut généralement résister à des températures plus élevées. Le PP a une grande variété d'utilisations mais est systématiquement utilisé pour les produits qui entrent en contact avec les aliments et boissons - tupperware, boîtes de yaourt, bouteilles de sirop, etc. PP fonctionne très bien avec Precious Plastic.

<b>Propriétés:</b> chimiquement inerte et hautement résistant, inodore et non toxique, peu sensible à l'humidité, résistant à la fatigue mécanique<br>
<b>Avantages:</b> similaire au PE mais en plus robuste, plus rigide, avec une température de fusion plus élevée et une densité plus faible, ses performances mécaniques, thermiques et électriques permettent un plastique d'ingénierie à faible coût<br>
<b>Inconvénients:</b> coût plus élevé que le PE, fragile en dessous de 0 ° C, haute perméabilité aux gaz, mauvaise résistance aux carburants, mauvaise résistance aux UV, entretient la flamme lorsqu'on le brûle<br>
<b>Sécurité:</b> bonne<br>
<b>Avertissement ⚠️</b> le PP lui-même (lorsqu'il ne brûle pas) n'est pas dangereux à utiliser, mais les additifs peuvent l’être. Il n'est pas possible de voir quel type d'additifs ont été utilisés dans la composition<br>
<b>Utilisations courantes:</b> pièces structurales, tuyaux, jouets, chaises, ustensiles de cuisine, boîtiers de DVD, emballages, films, textiles, tapis, cordes, filets<br>
<b>Meilleures façons d'utiliser avec les machines PP:</b> fonctionne bien avec toutes les machines! Nous adorons le polypropylène!

<b> Properties:</b> some properties here<br>
<b>Pros:</b> like PE but stronger, stuffer and higher temp and lower density, mechanical, thermal and electrical performance result in a low cost engineering plastic <br>
<b>Cons:</b> higher cost than PE, brittle below 0°C, high permeability to gases, poor resistance to fuels, poor UV resistance, keeps burning<br>
<b>Safety:</b> Good<br>
<b>Warning ⚠️</b> PP itself (when not burning) is not dangerous to use, however additives can be dangerous. It’s not possible to see what kind of additives are used in products.
<br>
<b>Common uses:</b> structural parts, pipes, toys, chairs, kitchenware, DVD cases, packaging, films, textile, carpets, rope, netting
<br>
<b>Best ways to use with PP machines: </b>Works well with all machines! We love polypropylene!

<img style="margin-left: 0; margin-top: 50px" src="../../assets/plastic/type-ps.svg" width="80"/>

### PS (6): Polystyrène
Le PS est plus communément connu sous le nom de  polystyrène expansé, mais apparaît également dans de nombreux autres produits. Le PS peut être recyclé, mais pas efficacement - le recyclage nécessite beaucoup d'énergie, ce qui signifie que peu d'endroits l'acceptent. Les tasses à café jetables, les boîtes à aliments en plastique, les couverts en plastique et la mousse d'emballage sont fabriqués en PS - cela fonctionne très bien avec Precious Plastic. C'est l'un des types de plastique les plus toxiques (donc une attention particulière s'il vous plaît!), mais il offre en même temps d’intéressantes propriétés esthétiques et haptiques car il est comparable au verre et peut être poli.

<b>Propriétés:</b> clair, brillant, dur, rigide<br>
<b>Avantages:</b> bon marché, rétrécissement important au refroidissement, bon isolant, bon à basse température<br>
<b>Inconvénients:</b> fragile, mauvaise résistance chimique et à l'usure<br>
<b>Sécurité:</b> moyenne<br>
<b>Avertissement ⚠️</b> en brûlant le PS, du styrène peut être libéré (toxique)<br>
<b>Utilisations courantes:</b> jouets, boîtiers de CD, diffuseurs de lumière, boîtiers électriques, couverts<br>
<b>Meilleures façons d'utiliser avec les machines PP:</b> extrusion, plaques, polissage (matériau semblable à du verre)

<img style="margin-left: 0; margin-top: 50px" src="../../assets/plastic/type-other.svg" width="80"/>

### MÉLANGE (7) 
Ce code est utilisé pour identifier d'autres types de plastique qui ne sont pas définis par les six premières catégories. Des plastiques tels que l'ABS, l'acrylique ou le polycarbonate sont inclus dans cette catégorie et peuvent être difficiles à recycler, mais Precious Plastic peut fonctionner avec une partie d’entre eux.

<b>Propriétés:</b> comme cela inclut de nombreux types de plastiques différents, les propriétés peuvent varier considérablement. Cela dépend donc du type de plastique dont il s'agit. Si vous pouvez l'identifier, vous pouvez le réutiliser mais s'il est mélangé, c'est le chaos!<br>
<b>Avantages:</b> il y a de bons plastiques avec lesquels travailler. ABS, PLA, Nylon<br>
<b>Inconvénients:</b> Difficiles à identifier car ils n'ont pas souvent leur propre étiquette. Donc, ça finit par être mélangé et c'est chaotique 🌪️<br>
<b>Sécurité:</b> Difficile de trouver la température de fusion, surtout si différents types sont mélangés. Ainsi, vous pouvez facilement le brûler, ce qui est assez dangereux<br>
<b>Avertissement ⚠️</b> Certains types de plastique inclus dans cette catégorie (comme le PC) dégagent des vapeurs très toxiques. Assurez-vous de vous informer sur le type de plastique avec lequel vous souhaitez travailler et de tester ses propriétés avant de le traiter en masse<br>
<b>Utilisations courantes:</b> PC (CD & DVD…), PLA (bioplastiques), ABS (filament d'impression 3D, jouets, produits électroniques…), PMMA (verre acrylique)

## Et rappelez-vous toujours:

![NE PAS mélanger le Plastique](assets/plastic/dontmixplastic.svg)

Il ne faut jamais mélanger différents types de plastique, car cela réduirait considérablement leur qualité et rendrait leur recyclage très difficile. De plus, lorsque différents types de plastiques sont fondus ensemble, ils ont tendance à se séparer en phases, comme l'huile et l'eau, et à prendre des couches, ce qui entraîne une faiblesse structurelle et des produits de qualité inférieure.

# Températures de fusion

Il existe donc différents types de plastique et l'une des raisons pour lesquelles il est si important de les séparer est la température de fusion. Ils atteignent tous un état liquide à une chaleur différente, donc pour fabriquer de nouveaux articles de haute qualité, il est important de savoir à quelle température chaque plastique fond, ainsi qu'à quelles températures différents types peuvent être moulés. Mais ne vous inquiétez pas, nous avons fait les tests pour vous! Vous pouvez en savoir plus sur les températures de fusion spécifiques dans la section <b>CRÉER</b> (voir <i>Conception</i>). Mais il est important de se souvenir des différents types de plastique, nous avons donc créé cette affiche pratique pour votre espace de travail ou votre studio. <br>

![Températures de Fusion](assets/plastic/melting-temperatures.jpg)
<br>

Ok, c'est tout pour les fondamentaux du plastique, nous espérons que vous aurez appris quelque chose ! N’hésitez pas à jeter un œil au chapitre suivant, où Jerry rentre plus en détail sur certains sujets.

<b>Vous souhaitez partager vos retours, discuter du plastique ou en savoir plus sur la communauté ? Allez sur le canal [#plastic](https://discordapp.com/invite/n5d8Vrr) sur Discord. Nous y discutons de plastique, de sécurité, de vapeurs et de propriétés des matériaux.</b>

<p class="note">NdT : Depuis le début du projet PP la communauté a beaucoup grandi. ☝️ Le lien ci-dessus vous emmène sur les salons "monde" 🌍 où on s'exprime en anglais. <b>Si vous préférez échanger en français sur ces sujets, il existe maintenant 👉<a href="https://discord.gg/Qa3wTrn">un Discord Francophone, à rejoindre par ici</a>! </b> Plus d'excuses pour ne pas vous présenter, vous et vos recherches 😉 </p> 

