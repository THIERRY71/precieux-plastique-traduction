---
id: get_featured
title: Get featured
sidebar_label: Get featured
---

<style>
:root {
  --highlight: #f7b77b;
  --hover: #f7b77b;
}
</style>


# Mettez vos annonces en vedette ✨

Nous aimons mettre en avant le bon travail, et promouvoir les vendeurs qui mettent de l'amour dans leur travail et leur apparition sur le Bazar.

**Si vous souhaitez que vos annonces soient mises en avant, assurez-vous de suivre les directives ci-dessous.**



## Pourquoi la mise en avant?

En mettant en avant une sélection d'articles, le Bazar vise à:

1. **Mettre en avant de belles annonces** afin de montrer aux utilisateurs la diversité et la qualité des articles disponibles sur le Bazar.
2. **Donner de la visibilité aux bons vendeurs** qui respectent les critères de sélection (voir ci-dessous) et les aider à augmenter leur engagement sur le Bazar.
3. **Augmenter les ventes** pour les vendeurs qui offrent une qualité et un service exceptionnels.


## Où faisons-nous la promotion?

- Sur la [page d'accueil du Bazar](bazar.preciousplastic.com)
- Dans les **newsletters du Bazar** destinées aux utilisateurs et followers
- Sur nos **médias sociaux** ([Instagram](www.instagram.com/realpreciousplastic), [Facebook](www.facebook.com/preciousplasticcommunity))


## Critères de sélection 🧐

Voici les critères que nous recherchons pour sélectionner les articles à mettre en avant:

Critère | Explication | Niveau de Priorité <span style="color:white"> </span>
--- | --- | ---
**Qualité du profil** | Nous souhaitons promouvoir les annonces des vendeurs qui apportent des efforts et du soin à leur profil et qui suivent le **[guide pour créer un compte de vendeur](https://community.preciousplastic.com/academy/business/Account_Setup)** | Essentiel
**Qualité de l’annonce** | Une liste sera sélectionnée si elle a été bien mise en place et si elle répond au **[guide relatif aux annonces](https://community.preciousplastic.com/academy/business/Image_Size_Guidelines)**. | Essentiel
Se positionner dans le **top 25** d’indicateurs pertinents | La liste doit se situer dans le top 25 de l'analyse des indicateurs importants. *Par exemple, pour les articles les plus vendus, nous baserons notre sélection sur les ventes de la période précédente.* | Essentiel
**Pas mis en avant récemment** | Une annonce ayant déjà été mise en avant dans les deux mois précédents ne pourra être sélectionnée. | Bon à avoir
**Diversité d’annonces** | Différents types d’annonces seront sélectionnés dans une section. *Par exemple: Nous ne sélectionnerons pas 2 moules de la même forme.* | Bon à avoir
**Diversité du vendeur** | Notre objectif est de présenter les annonces de différents vendeurs, afin de pouvoir également promouvoir  des projets dans divers pays. | Si possible

# Bonne vente ✌️

Bonne chance et n'hésitez pas à nous joindre directement pour de l’aide ou si vous avez des questions ou des idées pour augmenter votre visibilité et vos ventes sur le Bazar :)

You can find us daily on Discord on the [#🙌bazar-seller channel](https://discord.gg/2E93VxB3CD) or can send us an email at **bazar@preciousplastic.com**.
