---
id: injection
title: Construire une presse d'injection
sidebar_label: Presse d'injection
---

<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/qtZv96ciFIU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #f29094;
  --hover: #f29094;
}
</style>
<div class="videoChapters">
<div class="videoChaptersMain">

# Construire une machine d'injection

### Quelle est cette machine?


La presse d'injection dispose d’un débit de production élevé et relève d’une grande précision, mais la conception et la fabrication d'un moule nécessite un peu plus d'efforts au début. Vous serez cependant étonnés de ce qu’on peut créer avec. Le plastique broyé pénètre dans la trémie et est chauffé puis pressé à travers un long tube dans le moule. La couleur de sortie est souvent imprévisible lors du mélange des couleurs dans l’entonnoir, permettant des motifs magnifiques (et surprenants) qui peuvent ajouter de la valeur à vos produits uniques.

> Conseil d'expert: Investissez dans un bon moule. Un bon moule = un bon rendement :)

> Conseil d'expert: ne nouvelle version améliorée de la presse à injection est à retrouver dans les [how-tos](https://community.preciousplastic.com/how-to/injection-machine---designed-for-disassembly). Elle peut se démonter et comporte des améliorations au niveau de la sécurité. Si vous voulez en construire une nouvelle, jetez un coup d'œil à celle-ci avant de commander les pièces. 

</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00:07 Introduction
- 00:43 Hopper
- 02:01 Barrel
- 03:58 Nozzle
- 04:53 Framework
- 06:55 Electronics
- 10:30 How it works


</div>
</div>

# 📓 Informations techniques

📓 Type | Injection
--- | ---
💎 Version | 1.0
💰 Prix des matériaux neufs au Pays Bas | +/- €300
💰 Prix des matériaux en récup' au Pays Bas | +/- €150
⚖️ Poids | 23 kg
📦 Dimensions | 830 x 700 x 1300 mm
⚙️ Volume du corps de chauffe | 150 cm³
⚙️ Leverage | 3
⚙️ Pression à l'injection | 45 bars
⚙️ Taille max des moules| 360 x 330 mm
⏱ Injections /h | 10 - 30    
🔌 Voltage | 220V    
⚡️ AMP | 2.6A
| ♻️ Taille des paillettes en entrées                   | Petite, Moyenne  |


![Injection machine](assets/build/injection.jpg)


# 🌐 Modèle 3D
<iframe width="500" height="500" src="https://b2b.partcommunity.com/community/partcloud/embedded.html?route=embedded-viewer&name=Injection+Basic+V2.0&model_id=96645&portal=b2b&noAutoload=true&autoRotate=false&hideMenu=true&topColor=%23dde7ed&bottomColor=%23ffffff&cameraParams=false&varsettransfer=" frameborder="0" id="EmbeddedView-Iframe-96645" allowfullscreen></iframe>


# 🛠 Machines et compétences requises
Injecteuse  | Outils nécessaires | Compétences requises
--- | ---| ---
<img style="margin-left: 0;" src="../../assets/build/thumb-injection.jpg" width="100"/>  | - Perceuse à colonne <br> - Poste à souder <br> - Meuleuse d'angle | - Soudure (intermédiaire) <br> -Assemblage (intermédiaire) <br> - Electronique (intermediaire)


# ⚡️ Boîtier électronique
Explication des composants électriques à l'intérieur de cette machine. Plus d'informations et de schémas sont à retrouver dans le kit de téléchargement.

-  **Contrôleur PID**: le cerveau de la machine qui sert à régler les températures souhaitées. Il enverra de l'énergie aux éléments de chauffe jusqu'à ce que le PV (point variable) corresponde à la SV (valeur définie). Pour ce faire, il utilise les lectures du thermocouple et du SSR.
- **SSR**: le Solid State Relay est un interrupteur électronique qui s'ouvre ou se ferme en fonction du signal qu'il reçoit (du PID).
- **Thermocouple**: fondamentalement une sonde de température.
- **Élément chauffant**: élément chauffant qui s'adapte autour d'un tuyau.
- **Interrupteur d'alimentation**: interrupteur mécanique.
- **Indicateur lumineux**: LED s'allumant lorsque la machine est sous tension (souvent située sur l'interrupteur d'alimentation).
- **Câble d'alimentation**: câble d'alimentation domestique courant.

> Conseil d'expert: Voici un [ancien forum à propos de l'électronique (en anglais)](https://davehakkens.nl/community/forums/topic/the-big-electronics-topic/)


# 🛠 Trucs et astuces lors de la fabrication

- Prendre soin de souder autour des languettes de la trémie. La chaleur peut facilement déformer le tube et empêcher le glissement fluide du plastique.
- L'ajout d'un manchon isolé autour du tube augmente l'efficacité thermique et réduit les risques d’accidentellement toucher le tube lorsqu'il est chaud.
- Un levier circulaire est plus solide qu’un profil carré, réduisant la probabilité de le plier.


# ♻️ Capacité de traitement 
<b>Type:</b> HDPE, LDPE, PP, PS<br>
<b>Sortie:</b> 10 à 30 injections par heure selon le moule<br>

# ⚙️ Fonctionnement et maintenance

Vous pouvez créer très efficacement de beaux produits uniformes avec la machine à injection, mais cela demande un peu de travail en amont - la fabrication de moules, par exemple. Plus le moule est précis, plus votre production sera simple et uniforme. 

**Quelques conseils rapides **

### Mise en place

1. Allumer la machine et régler la température à 20°C de plus que la température souhaitée. S’assurer que le levier est en position la plus basse.
2. Attendre au moins 20 minutes.
3. Baisser la température et remplir le tube avec le plastique souhaité.
4. Attendre encore 15 minutes pour que le plastique fonde - le premier lot de plastique est utilisé pour nettoyer la machine et se débarrasser du plastique des sessions précédentes.
5. Presser la première fournée de plastique hors de la machine.
6. La machine est maintenant prête pour la production!

### Production 

1. La machine est maintenant chaude et prête à l'emploi avec vos moules
2. Remplir le baril avec le plastique choisi.
3. Appuyer sur le levier dans le tube.
4. Remonter le levier toutes les 5 à 10 minutes et ajouter plus de plastique.
5. Attendre au moins 10 minutes.
6. Maintenant pour le moule - dévisser la vis en laiton en bas.
7. Visser le moule (rapidement sinon le plastique commencera à couler!)
8. Une fois le moule fixé à la machine, abaisser le levier autant que possible, il ne faut pas avoir peur d’appliquer beaucoup de pression, la machine peut facilement supporter 100 kg.
9. Dévisser le moule de la machine.
10. Remonter le levier vers le haut.
11. Visser la vis en laiton en place.
12. Remplir la machine pour une nouvelle fournée.
13. Laisser le moule refroidir.
14. Ouvrir le moule une fois refroidi.

> Conseil d'expert:  selon le type de plastique, vous aurez besoin d'une ou deux personnes pour abaisser le levier. Avant d'injecter dans le moule, assurez-vous que le plastique dans le canon est complètement fondu afin de remplir toutes les zones du moule. Pour ce faire, faites chauffer la machine à quelques degrés de plus que d'habitude pour vous assurer que le plastique est complètement fondu.

### Refroidissement

1. Après utilisation, vider complètement le canon - cela facilite la tâche de la prochaine personne utilisant la machine.
2. Laisser la machine avec le levier complètement abaissé.
3. Éteindre la machine.


### Trucs et astuces lors de l'utilisation

1. Plus le plastique est chaud, plus les retassures sur le produit final seront importantes.
2. Nettoyer le plastique du moule tant qu'il est encore chaud, il sera d’autant plus difficile à nettoyer plus tard.
3. Appliquer une huile sur le moule pour aider au démoulage.
4. Garder le tube plein de plastique à tout moment, en en rajoutant un peu à chaque injection.
5. Après avoir utilisé la machine à injection, vider le plastique du tube.
6. Pour créer un processus efficace, il est conseillé de faire fonctionner la machine pendant quelques heures une fois qu'elle est allumée - arrêter et redémarrer la machine trop souvent rend le processus très inefficace.

# 🔓 Dépannage
* Si le plastique se bouche à l'extrémité du tube et ne sort pas, même en appliquant une pression plus élevée, augmentez la température de l’élément chauffant de la buse pour faire fondre complètement le plastique et libérer le bloc.<br>
Le produit obtenu peut coller au moule, ce qui rend sa libération difficile. Dans quel cas il faut réchauffer doucement le moule pour ramollir le plastique et aider à libérer la pièce.

# 🌦 Avantages et inconvénients
Avantages | Inconvénients
--- | ---
Facile à fabriquer     | Le processus peut être fastidieux|
Fonctionne sur 220V | Faible quantité de déchets recyclés|
Relativement peu cher 	 ||
Production par lots||


# 🌎 Construit par la communauté

<div class="j-slideshow">

![Community Shredder](../../assets/Build/community/community-injection4.jpg)

![Community Shredder](../../assets/Build/community/community-injection1.jpg)

![Community Shredder](../../assets/Build/community/community-injection2.jpg)

![Community Shredder](../../assets/Build/community/community-injection3.jpg)

![Community Shredder](../../assets/Build/community/community-injection5.jpg)

</div>

# 🙌 Liens Utiles (en anglais)
- [How-to: Injection Machine - Designed for disassembly](https://community.preciousplastic.com/how-to/injection-machine---designed-for-disassembly)<br>
- [Upgrade: Make a Quick Release Mould](https://community.preciousplastic.com/how-to/make-a-quick-release-opening-system-for-injection-moulds)<br>
- [Hack: Benchtop Injector](https://davehakkens.nl/community/forums/topic/benchtop-smaller-machines/)<br>
- [Hack: Injection Nozzle Refinement](https://davehakkens.nl/community/forums/topic/injectionextrusion-nozzle-refinement/)
- [How-to: Carabiner](https://community.preciousplastic.com/how-to/make-a-carabiner-cnc-vs-lasercut)<br>
- [How-to: Quick Release](https://community.preciousplastic.com/how-to/make-a-quick-release-for-the-extrusion-machine)<br>
- [How-to: Cast Aluminium Moulds](https://www.youtube.com/watch?v=5LhHUBz9uL0)<br>
- [How-to: CNC iPhone Case Mould](https://www.youtube.com/watch?v=ZYFoWP-3MYE)<br>
- [How-to: Geodesic Dome](https://community.preciousplastic.com/how-to/build-a-geodesic-dome)<br>
- [How-to: Precious Plastic Monash Machine](https://www.preciousplasticmonash.com/download-kit)<br>
- [How-to: Handplane](https://community.preciousplastic.com/how-to/make-a-handplane-simple-mould)<br>
- [How-to: Broom Hanger](https://community.preciousplastic.com/how-to/make-a-broom-hanger)<br>
- [Development: The Story Behind the Piranhaclamp](https://davehakkens.nl/community/forums/topic/the-story-behind-the-piranhaclamp/)<br>

**Si vous avez besoin d'aide, avez des questions ou cherchez quelqu'un à qui parler dans votre atelier empreint de solitude... Allez sur le canal [#build](https://discordapp.com/invite/XQDmQVT)sur Discord. Nous y discutons de la technique entourant la construction des machines.**
