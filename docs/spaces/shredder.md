---
id: shredder
title: Setup a Shredder Workspace
sidebar_label: Broyeur
---
<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/FFv4GR8ku38" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #37b4a3;
  --hover: #37b4a3;
}
</style>

# Espace Broyeur

<div class="videoChapters">
<div class="videoChaptersMain">

###  Cet espace de travail est destiné à recevoir les déchets plastiques des différents points de collecte que vous avez identifiés (associations, entreprises, particuliers) pour les broyer. Ces déchets plastiques deviennent alors une nouvelle matière première : les paillettes. 

Ces paillettes sont soit gardées dans votre atelier pour être transformées en nouveaux produits, soit vendues à d'autres ateliers pour la même utilisation.

Ok, vous voulez mettre en place un espace de travail de broyage. Fantastique ! 
L'espace de travail du broyeur est un élément crucial de l'univers de Precious Plastic. 
La mise en place d'un espace de travail peut se décomposer en trois étapes principales : 

1. <b>Rechercher:</b> à quoi penser lors du choix de votre espace 
2. <b>Planifier:</b> comment organiser son espace efficacement 
3. <b>Aménager:</b> tansformer une pièce vide en un espace de travail Shredder 

Chacune de ces étapes est expliquée plus en détail ci-dessous. 


</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00:00 Introduction
- 00:49 Shredder Choice
- 01:15 Sourcing Plastic
- 01:41 Shredding
- 03:36 Logistics


</div>
</div>


## 🔍 Rechercher

La première étape de la mise en place d'un espace de travail consiste à **trouver un local.** Lors de la recherche, il y a quelques considérations à garder à l'esprit. 


1. <b>Taille:</b>  Il est important de s'assurer que vous disposez de suffisamment d'espace pour vous déplacer librement avec vos machines et vos outils. L'espace de broyage que nous avons conçu est d'environ 5 x 6 mètres minimum - vous pouvez chercher plus grand, mais pas plus petit.

2. <b>Électricité:</b> Le broyeur peut fonctionner en monophasé ou en triphasé selon la version que vous envisagez de construire (v2 ou v4), alors assurez-vous de vérifier les spécifications du moteur que vous prévoyez d'utiliser et les types de raccordements électriques lors du choix de votre local. 

3. <b>Budget:</b> Tout en vous assurant que votre local répond à vos besoins fonctionnels, il est également important de garder à l'esprit comment il s'intégrera à votre budget (loyer, factures de consommation, assurance)

> Conseil d'expert:  parfois, les villes offrent des loyers réduits aux entrepreneurs et associations ayant des missions ciblées (économie circulaire, sensibilisation, etc), il vaut donc la peine de contacter les accélérateurs d’activités locaux et les représentants de votre municipalité pour voir s'il existe des opportunités comme celle-ci dans votre région. 

## ✍️ Planifier

Vous avez trouvé votre local. Vous pouvez maintenant commencer à planifier la manière dont vous voulez organiser les éléments de votre espace de travail en fonction de votre plan de masse. 

Chaque espace est différent, il est donc important de personnaliser votre disposition pour qu'elle fonctionne au mieux pour vous. 

Pour vous aider, nous avons inclus des plans de démarrage, des modèles CAO et un simulateur de plan personnalisé dans le kit de téléchargement, que vous pouvez utiliser pour explorer différentes dispositions.

![Shredder Workspace](assets/spaces_shredder.jpg)

Une fois que vous avez dessiné votre plan d'étage qui semble prometteur, testez-le sur site en traçant au sol  avec de la craie les limites de chaque élément. Modifiez si nécessaire jusqu’à satisfaction :-). 


## 🛠 Aménager

Maintenant que votre plan d'étage est finalisé, il est temps de transformer votre pièce vide en un espace de travail de broyage. 
Commencez par placer vos éléments les plus essentiels : broyeur, établi, outils de base...

Puis aménager le reste du mobilier à partir de là en commençant à voir ce qui convient le mieux à votre flux de travail. 

> _Conseil d'expert_:  il est préférable de garder les meubles de votre espace de travail sur roulettes afin de pouvoir les déplacer facilement au besoin.


Lorsque cela est possible, recherchez des pièces d'occasion. Si ce dont vous avez besoin n'est pas disponible d'occasion, achetez-le neuf ou construisez-le. Et privilégiez toujours la bonne qualité plutôt que le bon marché. 

Quelques éléments essentiels pour démarrer : 

  • Broyeur - bien sûr 

  • Récipients de stockage pour les déchets plastiques classés par type 

  • Récipients de stockage pour les paillettes de plastique - assurez-vous qu'ils sont solides et qu’ils rentrent sous le broyeur. Ainsi ils pourront être directement stockés après broyage. Gardez les types et les couleurs pures. 

  • Utensils type bout de bois ou tige en fer pour pousser le plastique dans le broyeur en cas de besoin.

  • Outils pour le nettoyage entre les lambeaux (???) - clés, clés allen…

  • Brosse à poils durs pour nettoyer les lames.


Une fois que vous avez acquis les bases, commencez à travailler avec le broyeur pour expérimenter et quantifier votre flux de travail. Ainsi vous pourrez conserver ou modifier l’espace afin d'être plus efficace.

 N’hésitez pas à décorer votre local avec des plantes, des affiches, un bon éclairage pour le rendre agréable à vivre. 

### Votre rôle dans l'univers Precious Plastic
| Ton badge |  Ta mission |
|----------|----------------------|
| <img src="../../assets/universe/badge-workspace.png" width="150"/>        |  __Atelier__ <br>Un atelier Precious Plastic  est un endroit où le plastique est transformé à partir de déchets en matériaux ou produits de valeur. Il existe cinq espaces de travail différents : Broyeur, Extrusion, Sheetpress, Injection et Mix. 

## 👋 Partager

Vous avez construit votre espace. Super ! Nous apprenons tous ensemble, alors assurez-vous de partager tous les hacks ou développements qui vous ont aidé à améliorer votre espace 🙂 


<b>Vous souhaitez partager vos commentaires, discuter de l'espace de travail Broyeur ou en savoir plus sur la communauté ? Rendez-vous sur le [Discord](https://discordapp.com/invite/p92s237). Ici, nous répondons aux questions et donnons des conseils sur la configuration de votre espace de travail et sur son fonctionnement. </b>
