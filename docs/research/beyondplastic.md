---
id: beyondplastic
title: Au-delà du Plastique
sidebar_label: Beyond Plastic
---

<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/oyDKboHoAxc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #798bc5;
  --hover: #798bc5;
}
</style>
<div class="videoChapters">
<div class="videoChaptersMain">

# Beyond Plastic

### Recherche d’alternatives biodégradables au plastique 🍊

On ne va pas se cacher, réaliser des objets en plastique recyclé, c’est formidable. Mais il faut, à terme, drastiquement réduire notre utilisation de matière plastique. C’est la raison pour laquelle nous avons créé __Beyond Plastic__ (“Au delà du plastique"), un projet visant à rechercher des matériaux alternatifs biodégradables au plastique, ainsi que des méthodes pour en faire des produits.

Dans cette partie, nous allons présenter la Bio Presse de Beyond Plastic - une presse à chaud puissante qui permet de transformer une variété de matières organiques (telles que les déchets de cuisine) en objets biodégradables comme des assiettes, des bols ou des gobelets. On vise d’abord à produire des objets à usage unique, car on veut se débarrasser en premier des objets en plastique considérés comme “jetables”! Woohoo! 🎉

</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00:07 Introduction
- 00:43 Upsides
- 02:29 Start experiment
- 04:13 Downsides
- 06:56 Finish experiment
- 09:42 bio vs biodegradable
- 11:12 Beyond plastic explained
- 12:57 Grandpa


</div>
</div>
<br><br>

![Beyond Plastic Bowl](assets/Research/BP_bowl.jpg)

## Commençons

Il faut trois choses pour produire de magnifiques objets avec la Bio Presse: la presse à chaud elle-même, les moules, et les matériaux. On détaille chaque partie ci-dessous, et toutes les ressources numériques sont à retrouver dans le kit de téléchargement.

> _Conseil d'expert_: c’est un long processus que d'apprendre à connaître son matériau. Ça ne fonctionne généralement pas au premier essai (ni même au deuxième) - et c’est tout à fait normal ! Dans ce cas, il faut faire preuve de persistance et de patience, mais dès que tu en auras assez appris sur ton matériau, tu te seras fait un ami pour la vie !

# La Presse à Chaud (ou BioPresse)

<iframe width="560" height="515" src="https://www.youtube.com/embed/b2aRrWCxaX4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Construire la Bio Presse n’est pas plus compliqué que les autres machines, mais si tu n’as jamais construit de machine avant cela ne sera pas simple. Il vaut mieux avoir des gens dans le coin pour t’aider en cas de problème. Alors demande à tes amis, ou regarde sur la carte !

## ♻️ Spécifications générales

| Version               | 1                                                                                     |
| --------------------- | ---------------------------------------------------------------------------------------------- |
| Matériel requis       | Beaucoup de métal, un cric hydraulique de 20t, une cartouche chauffante, un contrôleur PID et du fil électrique |
| Coût aux Pays-Bas     | 280 € (+ moules non inclus)                                                                    |
| Poids                 | 110kg                                                                                          |
| Dimensions            | HxWxD : 1300x320x480mm  (avec table dépliée: 1300x1360x480mm)                                  |
| Pression Possible     | 20t                                                                                          |
| Surface de pression max | Largeur 240mm; Profondeur 400mm (espace inter-pieds)                                                     |
| Énergie utilisée      | 2 éléments chauffants de 400W chacun                                                                   |

# 🛠 Machines et compétences requises

| Bio Press                                                                              | Machines needed                                                                                                                                                                         | Compétences                                                                                                                   |
| -------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------- |
| <img style="margin-left: 0;" src="../../assets/Research/thumb_biopress.jpg" width="100"/> | - Meuleuse en angle <br> - Poste à souder <br> - Perceuse à colonne <br> - Mèches <br> - kit de filetage <br> - Clés Allen <br> - Kit d'assemblage électronique (outils de sertissage, tournevis...) | - Couper / Meuler (intermédiaire) <br> - Souder (intermédiaire) <br> - Assembler (intermédiaire)<br> - Electronique (intermédiaire) |

### Améliorations non-incluses dans la vidéo:

C’est la première version de la Bio Presse et on a déjà trouvé des améliorations facilitant l’utilisation de la machine:

1. La première consiste à ajouter un système de rails pour faire glisser les moules - cela rend la tâche d’insertion de la partie inférieure du moule beaucoup plus aisée et lui permet d’être parfaitement alignée avec celle du haut. La conception est assez simple, on a utilisé des baguettes avec un profil en L pour faire le cadre, et une boîte en bois pour y mettre le moule. Les plans sont à retrouver dans le kit de téléchargement.

2. La seconde a été l’agencement de l’espace de travail où nous y avons ajouté un déshydrateur plutôt basique, un chouette système de tri avec des pots en verre récupérés chez les restaurateurs autour de nous, et avons même réfléchi à un moyen de récupérer de la matière première fraîche dans le quartier. Si tu es intéressé, n’hésite pas à aller voir notre vidéo de guide d’utilisation, toutes les informations sont dans le [kit en téléchargement](https://github.com/ONEARMY/precious-plastic-kit/releases/download/V4.1/precious-plastic-kit.zip).

# 🎈 Moules

![Beyond Plastic Mould](assets/Research/BP_mould.jpg)

On a inclus trois moules différents dans le kit de téléchargement:

1. Un petit moule à panneau, pratique pour tester des matériaux et faire une librairie d’échantillons

2. Un moule à gobelet

3. Un moule à bol

Les moules à gobelet et à bol sont plus complexes à fabriquer, mais on a essayé de les rendre le plus universel possible, pour convenir à différentes situations. C’est la raison pour laquelle on a conçu un moule à réaliser en fabrication numérique qui peut être simplement commandé chez un prestataire, et un autre qui peut être construit avec un tour à métal. Il est certainement possible de faire les deux sur un tour, mais pour le moule à bol il en faudrait un assez grand. Celui que nous avons à l’atelier PP à Eindhoven ne l’est pas assez en tout cas. 😉

Tous les fichiers et dessins des moules sont à retrouver dans notre kit de téléchargement. Libre à tout le monde de les utiliser, les modifier, et les développer.

> Conseil d’expert: Pour un objet durable, fais des murs épais. Pour un objet léger, fais des murs plus fins 🤓

Spécifications des moules:

| Type         | Dimensions       | Prix en Aluminium aux Pays-Bas | Moyens de fabrication|
| ------------ | ---------------- | ---------------------------------- | ------------- |
| Moule à échantillon | 100 x 200 x 50mm | 41 €                        | CNC-cut       |
| Moule à bol  | Ø 220 x 90 mm    | 116 €                              | CNC-cut       |
| Moule à gobelet    | Ø 100 x 125 mm   | 40 €                         | fait au tour  |

Matériel utilisé pour la fabrication des moules:

• CNC / Tour à métal
• Perceuse à colonne
• Mèches de perçage
• Kit de filetage
• Clés Allen


# 🍊 Matériaux

![Beyond Plastic Materials](assets/Research/BP_material.jpg)

Quelques matériaux ont été testés dans la presse, et jusqu’ici on a l’impression que les matériaux contenant de la cellulose devraient, en principe, tous fonctionner. Toutefois, pour des objets plus grands, il convient d’ajouter un liant naturel dans le mélange pour plus de rigidité. On peut utiliser de l’amidon, de l’agar agar, de la gomme de xanthane, ou encore de la gélatine. Si tu ne veux pas utiliser des matériaux frais, sois créatif: il y a de l’amidon dans pratiquement toutes les racines végétales, y compris dans les épluchures. On peut aussi s’amuser à trouver diverses choses comme agent de liaison, tel que du pain rassis, ou même des algues ! Des insectes morts peuvent aussi être une source de liant naturel 

On ne le dira jamais assez, **apprendre à connaître les matériaux avec lesquels on travaille est un long processus. Il ne faut pas être frustré si les premiers essais sont un échec!** ;)

Et voici la liste des matériaux qui ont été testés jusqu’ici:
• Son de blé <br>
• Marc de café<br>
• Résidus de thé<br>
• Épluchures de pommes de terre<br>
• Pelures d'orange<br>
• Pain rassis<br>
• Pelures de gingembre<br>
• Haricots noirs<br>
• Noyaux d'avocat<br>
• Graines de sésame<br>
• Aiguilles de pin<br>
• Feuilles<br>
• Feuilles de bambou (cousues ensemble)<br>
• Coquilles d'œuf (décoratives)<br>
• Fleurs (décoratives)<br>
• Pelures d'oignon (expérimental)<br>
• Cheveux (expérimental)<br>


## Comment ça fonctionne:

Plus spécifique (un peu de chimie, mais pour débutant)

Cette partie est destinée aux personnes intéressées par la chimie derrière le processus qu’on utilise. Mais n’hésitez pas à me corriger si j’ai tort! Je ne suis pas un chimiste, mais un designer, et ceci représente tout ce que j’ai compris jusqu'ici.

Si on chauffe et qu’on presse un matériau contenant de la cellulose dans un moule fermé, l’énergie des molécules d’eau contenues dans la matière (qui part autrement en vapeur d’eau)  amène à créer des nouvelles liaisons au sein du réseau de cellulose. C’est la raison pour laquelle les matières premières comme le marc de café, qui contient une bonne quantité de cellulose, parviennent à se solidifier sans utilisation d’un agent de liaison. On obtient un résultat à l’aspect plutôt minéral comme tu peux le constater:

![Beyond Plastic Materials](assets/Research/BP_sample.jpg)

Pour que ça fonctionne il faut un moule étanche, car sinon l’énergie peut s’échapper sous forme de vapeur d’eau, auquel cas on se retrouverait avec une matière pas totalement liée. C’est pourquoi nos moules sont conçus avec une tolérance assez faible, et sont cloisonnés de sorte à permettre la compression.

Pour tous les liants naturels mentionnés ci-dessus: ils agissent tous à une température plutôt basse (l’amidon à 60°C par exemple), et généralement en combinaison avec de l’eau. Ils peuvent donc être très utiles dans le cas où le moule n’est pas totalement hermétique, ou pour toute autre raison qui empêcherait la liaison de la cellulose.

> Conseil d’expert: mélange des matériaux à grain fin avec des longues fibres pour obtenir un résultat lisse ET rigide! ☕️🍵

## Les prochaines étapes:

• Vernis biodégradable
• Colorants et teintures naturels
• Trouver d’autres usages à ces matériaux (fabrication de meubles par exemple)


# Comment fabriquer un bol

<iframe width="560" height="515" src="https://www.youtube.com/embed/UD57zOGqAPs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<b>Tu veux partager tes expériences, discuter du projet Beyond Plastic ou apprendre de la communauté? Fonce vers le canal #beyond plastic sur Discord. On y discute les façons d’inventer des matériaux alternatifs au plastique.</b>
