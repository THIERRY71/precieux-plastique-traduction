---
id: injection
title: Lancer un atelier d'injection
sidebar_label: Injection
---

<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/hIlmxuz1ZKs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #37b4a3;
  --hover: #37b4a3;
}
</style>

# Espace Injection

<div class="videoChapters">
<div class="videoChaptersMain">

###  Cet Espace Precious Plastic transforme les déchets plastiques en les injectant dans des moules en acier ou en aluminium pour réaliser des petites productions d'objets très précis comme des pots ou des tuiles. 

Vous souhaitez configurer un espace de travail d'injection. Choix intelligent !
Il est relativement peu coûteux de commencer avec l’injection. Vous accédez très vite à des résultats satisfaisants dans la création d’objets de qualité pour vos clients. 
La mise en place d'un tel espace de travail peut se décomposer en trois étapes principales : 

1. <b>Rechercher:</b> à quoi penser lors du choix de votre espace 
2. <b>Planifier:</b> comment organiser son espace efficacement.
3. <b>Aménager:</b>  transformer une pièce vide en un espace de travail d’injection

Chacune de ces étapes est expliquée plus en détail ci-dessous. 

</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00:00 Introduction
- 00:49 Injection Machine
- 01:01 Sourcing and Shredding Plastic
- 01:38 Shredding Time
- 02:16 Injecting Process
- 03:56 Logistics

</div>
</div>

## 🔍 Rechercher

La première étape de la mise en place d'un espace de travail consiste à **trouver un local.** Lors de la recherche, il y a quelques considérations à garder à l'esprit. 

1. <b>Taille:</b> Il est important de s'assurer que vous disposez de suffisamment d'espace pour vous déplacer librement avec vos machines et vos outils. L'espace d’extrusion que nous avons conçu est d'environ 5 x 6 mètres minimum - vous pouvez chercher plus grand, mais pas plus petit.

2. <b>Électricité:</b> La machine à injection fonctionne en monophasé, mais le broyeur fonctionne parfois en triphasé, alors assurez-vous de vérifier les spécifications du moteur que vous prévoyez d'utiliser lors du choix de votre espace et les installations électriques du local..

3. <b>Budget:</b> out en vous assurant que votre local répond à vos besoins fonctionnels, il est également important de garder à l'esprit comment il s'intégrera à votre budget (loyer, factures de consommation, assurance)

> Conseil d'expert: arfois, les villes offrent des loyers réduits aux entrepreneurs et associations ayant des missions ciblées (économie circulaire, sensibilisation, etc), il vaut donc la peine de contacter les accélérateurs d’activités locaux et les représentants de votre municipalité pour voir s'il existe des opportunités comme celle-ci dans votre région. 

## ✍️ Planifier

Vous avez trouvé votre local. Vous pouvez maintenant commencer à planifier la manière dont vous voulez organiser les éléments de votre espace de travail en fonction de votre plan de masse. 

Chaque espace est différent, il est donc important de personnaliser votre disposition pour qu'elle fonctionne au mieux pour vous. 

Pour vous aider, nous avons inclus des plans de démarrage, des modèles CAO et un simulateur de plan personnalisé dans le kit de téléchargement, que vous pouvez utiliser pour explorer différentes dispositions.

![Injection Workspace](assets/spaces_injection.jpg)

Une fois que vous avez dessiné votre plan d'étage qui semble prometteur, testez-le sur site en traçant au sol  avec de la craie les limites de chaque élément. Modifiez si nécessaire jusqu’à satisfaction :-). 


## 🛠 Aménager

Maintenant que votre plan d'étage est finalisé, il est temps de transformer votre pièce vide en un espace de travail d’injection. 

Commencez par placer vos éléments les plus essentiels : injecteuse, établi, outils de base...  Puis aménager le reste du mobilier à partir de là en commençant à voir ce qui convient le mieux à votre flux de travail.

> Conseil d’expert : il est préférable de garder les meubles de votre espace de travail sur roulettes afin de pouvoir les déplacer facilement au besoin.

Lorsque cela est possible, recherchez des pièces d'occasion. Si ce dont vous avez besoin n'est pas disponible d'occasion, achetez-le neuf ou construisez-le. Et privilégiez toujours la bonne qualité plutôt que le bon marché. 

Quelques éléments essentiels pour démarrer : 
    • Injecteuse - bien sûr 
    • Récipients de stockage pour les paillettes de plastique classés par types et couleurs. 
    • Moule simple – il existe de nombreuses méthodes et matériaux que vous pouvez utiliser pour fabriquer un moule - découpe laser, CNC, impression 3D, aluminium, acier, acrylique. Consultez les <a href="https://community.preciousplastic.com/">tutoriels</a> pour vous inspirer.     
    • Outils de base - clés, couteau à mastic, lunettes de sécurité, marteau, ciseau, gants résistants à la chaleur, minuterie, pelle, truelle. 
    • Ventilation – masque filtrant, panier de ventilation, regardez la <a href="https://community.preciousplastic.com/academy/plastic/safety">vidéo de sécurité</a>    •  pour en savoir plus. 
    • Table de travail. 
    • Conteneur de chutes. 

Une fois que vous avez acquis les bases, commencez à travailler avec l’injecteuse pour expérimenter et quantifier votre flux de travail. Ainsi vous pourrez conserver ou modifier l’espace afin d'être plus efficace.

N’hésitez pas à décorer votre local avec des plantes, des affiches, un bon éclairage pour le rendre agréable à vivre., pour vous comme pour vos visiteurs ! 


### Votre rôle dans l'univers Precious Plastic
| Votre badge  |  Votre mission |
|----------|----------------------|
| <img src="assets/universe/badge-workspace.png" width="150"/>        |  __Atelier__ <br> Un atelier Precious Plastic  est un endroit où le plastique est transformé à partir de déchets en matériaux ou produits de valeur. Il existe cinq espaces de travail différents : Broyeur, Extrusion, Sheetpress, Injection et Mix. |

## 👋 Partager

Vous avez construit votre espace. Super ! Nous apprenons tous ensemble, alors assurez-vous de partager tous les hacks ou développements qui vous ont aidé à améliorer votre espace 🙂 

<b>Vous souhaitez partager vos commentaires, discuter de l'espace de travail Injection ou en savoir plus sur la communauté ? 
Rendez-vous sur le [Discord](https://discordapp.com/invite/p92s237) de la communauté.Ici, nous répondons aux questions et donnons des conseils sur la configuration de votre espace de travail et sur son fonctionnement. </b>
