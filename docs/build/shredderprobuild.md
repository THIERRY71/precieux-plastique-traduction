---
id: shredderprobuild
title: Construire le Broyeur Pro
sidebar_label: - le Construire
---

<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/4GWYJhAd-R0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #f29094;
  --hover: #f29094;
}
</style>



<div class="videoChapters">
<div class="videoChaptersMain">

# Construire le Broyeur Pro

Vous voulez donc broyer du plastique ? Génial ! Il existe deux broyeurs différents : le petit broyeur originel et le broyeur Pro. La version Pro est présentée ici - il est plus robuste, abouti et productif que le petit, mais aussi plus complexe à construire. Toutefois, si vous êtes familier avec la construction de machines, c'est très accessible, et vous pouvez également acheter des pièces sur le bazar. Ou la machine entière ! Vous pourriez même les vendre vous-même. Le Broyeur est en fait la machine la plus demandée autour du globe !

> Conseil d'expert:  la construction d'un Broyeur Pro nécessite des outils et compétences un peu plus avancés que le broyeur normal. Les outils eux-mêmes sont assez basiques et peuvent se trouver partout. Mais les tolérances sont nettement plus strictes. Vous devez donc vous assurer que vos outils et compétences sont à la hauteur.


</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00:07 - Intro
- 01:34 - Frame
- 03:45 - Shafts
- 08:16 - Hopper
- 08:57 - Sieve
- 10:44 - Electronics
- 11:50 - Motor and gearing
- 13:15 - Full assembly
- 16:16 - How to run



</div>
</div>



# 🛠 Machines et compétences requises
Construire le Broyeur Pro | Outils nécessaires 	|		Compétences requises
--- | ---| ---
<img style="margin-left: 0;" src="../../assets/build/thumb-shredder-pro.jpg" width="150"/>  | -  Tour à métal	 <br> - Perceuse à colonne <br>- Fraiseuse <br> - Poste à souder <br> - Clé torq <br> - Taraudeuse manuelle M16 | - Soudure (intermédiaire) <br> - Usinage (expert) <br> - Assemblage (expert)<br> - Electronique (expert)


# 🔩 Matériaux et pièces

Vous devez fabriquer et acheter des pièces. Vous pouvez retrouver la liste complète des composants dans le kit en téléchargement. C'est une liste qui couvre l'ensemble du Broyeur Pro. Ci-dessous, vous pouvez trouver la liste des pièces à acheter dans le commerce, avec des détails sur les endroits où les trouver.

### Option moteur et explication

Toute l'énergie pour broyer le plastique provient du moteur électrique. Plus d'énergie signifie plus de productivité et de plus petits flocons. Un broyeur à double arbre fonctionne à faible vitesse et avec un couple élevé, de sorte que le moteur doit avoir ces exigences.

Pour choisir un moteur triphasé adaptable, vous devez vérifier les éléments suivants:

- Puissance Nominale
- Vitesse de sortie
- Couple
- Facteur de charge

### Puissance Nominale

La puissance nominale est une valeur directement liée au courant nominal. La plupart des moteurs peuvent fonctionner à cette puissance en continu (en fonction du facteur de sécurité choisi, de la température ambiante, de l'humidité, etc.).

Cependant, pendant un court laps de temps, le moteur sera en mesure de fournir une puissance plus élevée grâce à un courant plus élevé. Cela peut généralement s’observer au début de la rotation ou avec des pièces plus difficiles à broyer.

Même si le moteur est capable de fonctionner à une puissance supérieure, cela pourrait l'endommager. Nous avons constaté qu'aucun moteur ne devrait fonctionner à plus de 1,6 fois le courant / puissance nominal, et cela devrait être contrôlé par le boîtier électronique. Voir le paragraphe de recommandation sur le fait d’adapter l'alimentation en fonction des besoins.

### Vitesse et couple

Dans un moteur triphasé, la vitesse du moteur dépend de la fréquence et du nombre de pôles du moteur.
- Vitesse théorique (tr / min ou rpm)
- nombre de pôles du moteur : de 2 à 12

| Fréquence (Hz) | 2    | 4    | 6    | 8   | 10  | 12  |
|----------------|------|------|------|-----|-----|-----|
| 50             | 3000 | 1500 | 1000 | 750 | 600 | 500 |
| 60             | 3600 | 1800 | 1200 | 900 | 720 | 600 |

Vitesses synchrones des moteurs (RPM)

*La vitesse réelle sera probablement inférieure en raison des charges, frottements ...*

La vitesse peut ensuite être réduite grâce à une boîte de vitesses, qui est définie par un rapport de boîte de vitesses. Rapport = vitesse d'entrée / vitesse de sortie. Plus la vitesse sera élevée, plus la productivité sera élevée. Un broyeur à double axe est un broyeur qui fonctionne à faible vitesse. Nous vous recommandons de rechercher une boîte de vitesses qui amènera la vitesse de sortie entre 15 et 25 tours par minute.

Le couple est une relation entre la vitesse de sortie obtenue et la puissance du moteur. Plus le couple sera élevé, plus le broyeur sera fort. Pour obtenir un couple élevé, vous pouvez augmenter la puissance du moteur ou augmenter le rapport de boîte de vitesses. Sachez que l'augmentation du rapport de boîte de vitesses réduira la vitesse et la productivité. Pour ce broyeur, nous recommandons un couple nominal de 1100N.m au minimum et 2000N.m comme objectif sérieux.


### Facteur de charge du moteur

Le facteur de charge du moteur est le pourcentage de surcharge que le moteur peut supporter pendant de courtes périodes lorsqu'il fonctionne normalement dans les tolérances de tension correctes. Votre boîte de vitesses et votre moteur ont un facteur de charge qui peut être différent. Dans tous les cas, assurez-vous que le facteur de charge des deux éléments est d'au moins 1, sinon votre motoréducteur s’en trouvera rabaissé.

Un facteur de charge moteur plus élevé peut être utile pour :

- Pallier à l’imprévisibilité des besoins en puissance du système intermittent
- Allonger la durée de vie de l'isolation en abaissant la température à charge nominale
- Affronter des surcharges intermittentes ou occasionnelles
- Température ambiante supérieure à 40 ° C
- Tensions d'alimentation faibles ou déséquilibrées

Un facteur de charge de la boîte de vitesses plus élevé peut être utile en cas de :

- Températures élevées
- Charges de choc ou vibrations extrêmes
- Charges non uniformes
- Charges cycliques

Plus le facteur de sécurité est élevé, plus la durée de vie est élevée. Vous pourriez aussi bien compenser et avoir un moteur moins puissant pour cette raison.

### Propositions moteurs

**2.2kW - 16 rpm - 1200N.m**:  : le plus petit moteur suggéré, conviendra au petit plastique domestique, mais le moteur limitera le fonctionnement du broyeur et ne fonctionnera donc jamais à sa pleine capacité.

**3kW - 18 rpm - 1500N.m**: probablement un bon compromis coût / efficacité.

**4kW - 18 rpm - 2000N.m**:  vous pourrez faire fonctionner le broyeur à sa pleine capacité.

**5,5kW - 22 rpm - 2300N.m**: moteur à très longue durée de vie car il tournera en deçà de sa capacité ; il pourrait valoir la peine pour un broyeur à haute productivité (vitesse plus élevée pour limiter le couple).

### Option de couplage et explication

Les couplages moteur ont généralement un couple nominal et un couple maximal qu'ils peuvent supporter. Le couple nominal est la valeur de couple que le couplage peut supporter sans dommage. Le couplage peut supporter un couple plus élevé pendant une courte période, mais toujours garder le seuil sous le couple maximum. La durée de vie des composants sera réduite si le couplage fonctionne fréquemment au-dessus du couple nominal.

Si votre moteur a un couple nominal d'environ 2000 Nm, un couplage HRC 230 fonctionnera correctement. Même si vous choisissez un moteur avec un couple plus faible, nous vous recommandons de surdimensionner légèrement le couplage pour une durabilité accrue.

| Reference          | Couple nominal (Nm) | Couple maximal (Nm) | Notes                                             |
|--------------------|---------------------|-----------------|---------------------------------------------------|
| HRC 230 Coupling   | 2000                | 5000            | Avantages: moins cher <br>Cons: smaller misalignments           |
| F140 Tyre coupling | 2325                | 5642            | Pros: higher misalignment <br>Cons: more expensive |

Nous vous recommandons de suivre les instructions de montage du fabricant. Le non-respect de cette exigence peut entraîner des dommages ou une usure précoce.

### Choix des engrenages

Les engrenages transmettent la puissance entre les deux arbres. Les broyeurs à double arbre fonctionnent à faible vitesse et à couple élevé, de sorte que les engrenages doivent être calculés pour soutenir ce couple. Nous avons recommandé les spécifications d'équipement suivantes :

| Gear type | Module | No of teeth | Width (mm) | Hole diam. (mm) | Keyway (mm) |
|-----------|--------|-------------|------------|-----------------|-------------|
| Spur gear | 6      | 18          | 60         | 45              | 14 x 9      |
| Spur gear | 6      | 20          | 60         | 45              | 14 x 9      |

Il est recommandé d'utiliser des engrenages à dents endurcies.

La distance entre les deux doit être de 114 mm. De nombreux composants, comme les lames ou la boîte, dépendent de cette distance. Si les spécifications de l'équipement changent, la distance doit être maintenue fixe ou une reconception des lames et de la boîte sera nécessaire.

### L'arbre
L'arbre est constitué d'une barre hexagonale de 50 mm fraisée à un diamètre de 45 mm. Nous recommandons d'utiliser de l'acier de qualité moyenne à élevée avec une limite d'élasticité minimale de 350 MPa (nous avons utilisé de l'acier C45).

### Les roulements

Nous avons utilisé des roulements UCFL209 :
  - Taille de l'arbre 45 mm
  - Espacement de fixation 148 mm
  - Diamètre du trou de fixation 19 mm

Nous utilisons un boulon M16 pour les fixer : un adaptateur est nécessaire.

### Pièces découpées au laser

Toutes les pièces découpées à la laser sont en acier doux. Elles ne nécessitent pas de matériau spécifique. Cependant, nous avons utilisé de l'acier résistant à l'usure (Hardox 400) pour les lames et les lames fixes (pas les entretoises), pour les rendre plus durables.

# ⚡ Boîtier électronique

### Composants électriques
  - 1 x interrupteur général triphasé
  - 1 x interrupteur d'arrêt d'urgence avec 3 connexions NC
  - 1 x limiteur de courant triphasé, 6,8 ampères
  - 2 x contacteurs triphasés, 3 connexions NO et 1 NC
  - 1 x alimentation 5V 500mA

### Lien du schéma
https://github.com/davehakkens/shredder-reverse/blob/master/Schematic.pdf

### Composants électroniques
  • 1 x Arduino nano
  • 1 x Module relais 5V 2 canaux 250VAC
  • 1 x Module de capteur à effet Hall ACS712 30A
  • 1 x Pont redresseur 1A (DF10-G)
  • 1 x Condensateur 100uF 10V
  • 2 x Résistances 10k 1 / 4W
  • 1 x Résistance 100k 1 / 4W
  • 1 x Condensateur 2,2mF 10V
  • 1 x Interrupteur à bascule 3 positions

### Lien du code Arduino

https://github.com/davehakkens/shredder-reverse

# 👌  Trucs et astuces lors de la fabrication

Il existe deux types de lames différentes. Les lames à 13 dents nécessitent plus de puissance moteur mais produisent des flocons plus petits, tandis que les lames à 6 dents nécessitent moins de puissance mais produisent des flocons plus longs. L'adhérence devrait être légèrement meilleure avec des lames à 6 dents, mais cela dépend de la forme et du matériau.

| Lames    | Puissance requise | Taille des Flocons | Grip   | Output          |
|----------|----------------|------------|--------|-----------------|
| 6 dents  | + basse        | + grande   | meilleur | légèrement moins  |
| 13 dents | + haute        | + petite    | bon  |légèrement plus  |

- Pour avoir un bon équilibre dans les performances du broyeur, nous recommandons d'utiliser 13 lames de dents sur l'arbre connecté au moteur et 6 lames de dents sur l'arbre opposé. Mais vous pouvez choisir et configurer le broyeur à votre guise.

- L'écart entre les lames est de 0,25 mm. Pour atteindre ce niveau de précision, il est bon de garder toutes les pièces propres pendant le montage de la machine.

- Les lames sont fixées à l'arbre par deux écrous de chaque côté. Il est recommandé de serrer les écrous (100 Nm au moins) pour empêcher le plastique de pénétrer entre les lames et les entretoises.

- Les arbres sont assez longs et lourds. Si vous n’avez pas accès à un tour de bonne qualité, nous vous recommandons de les commander auprès d'un professionnel. Commandez plusieurs entretoises pour le réglage de l'assemblage.

- Souder les tubes de la boîte, pour un nettoyage plus rapide (soudure non structurelle)

- Fixez le moteur: vous utiliserez probablement un moteur différent de celui du modèle 3D. Vous devrez donc modifier le cadre.

# Options Supplémentaires

### Trémie de sécurité

Nous avons conçu une version entièrement fermée de la trémie pour empêcher le plastique de sauter et rendre l’utilisation plus sûre pour les personnes. Les dessins sont accessibles dans le kit en téléchargement pour les découper au laser. Assurez-vous de choisir la trémie souhaitée avant d'envoyer les fichiers en fabrication.

### Tamis différents

Vous pouvez utiliser le broyeur sans tamis, mais afin d'atteindre différentes normes de taille de flocons - voir le chapitre d'entrée et de sortie - nous avons fourni différentes conceptions de tamis à découper. Vous pouvez obtenir vos propres plaques perforées, mais nous avons conçu ces tamis pour qu'ils soient de tailles parfaites pour nos normes, avec une concentration de trous plus dense pour plus d'efficacité et avec une ligne pour un soudage facile.

**Si vous avez besoin d'aide, avez des questions ou cherchez quelqu'un à qui parler dans votre atelier empreint de solitude... Allez sur le canal [#build](https://discordapp.com/invite/XQDmQVT)sur Discord. Nous y discutons de la technique entourant la construction des machines.**
