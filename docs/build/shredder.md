---
id: shredder 
title: Construire le Broyeur 
sidebar_label: Broyeur
---

<div class="videocontainer"> <iframe width="800" height="400" src="https://www.youtube.com/embed/VFIPXgrk7u0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </div>

<style> :root { --highlight: #f29094; --hover: #f29094; } </style>

<div class="videoChapters"> <div class="videoChaptersMain">

# Construire le Broyeur

### Quelle est cette machine ?

Le broyeur est l'épine dorsale de Precious Plastic, il permet de déchiqueter le plastique en petits flocons qu’on peut facilement laver, stocker et transporter vers d'autres ateliers Precious Plastic pour en faire de beaux et précieux produits. Bien sûr, on peut également utiliser le plastique soi-même ou vendre ce plastique propre et trié à l'industrie (qui l'achète 8 à 10 fois plus cher que le plastique non broyé !).

> Conseil : Si le plastique est broyé par couleurs, on peut avoir plus de contrôle sur l'apparence et la sensation que renvoie les objets créés, ajoutant de la valeur au matériau

</div> <div class="videoChaptersSidebar">

### Video Chapters

* 00:07 Introduction
* 00:46 Shredding box
* 04:18 Power supply
* 06:35 Framework
* 08:07 Hopper
* 09:31 How it works

</div> </div>

# 📓 Informations techniques

| 📓 Type | Single Shaft Shredder |
|---------|-----------------------|
| 💎 Version | 2\.0 |
| 💰 Prix nouveaux matériaux aux Pays Bas -2015 | \+/- €500 + moteur |
| 💰 Price matériaux de récup' aux Pays Bas - 2015 | \+/- €400 |
| ⚖️ Poids | 90 kg |
| 📦 Dimensions | 280 x 600 x 1142 mm |
| ⚙️ Blade width | 5 mm, 6 mm |
| 🔌 Voltage | 380V |
| ⚡️ AMP | 5\.8A |

* Nominal Power | 1.5 kW minimum
* Nominal Torque | 300 Nm minimum
* Output Speed | ±70 r/min

![Shredder V3](assets/build/shredderv3.jpg)

# 🌐 Modèle 3D

<iframe width="500" height="500" src="https://b2b.partcommunity.com/community/partcloud/embedded.html?route=embedded-viewer&name=Shredder+Basic+V2.0&model_id=96649&portal=b2b&noAutoload=true&autoRotate=false&hideMenu=true&topColor=%23FFFFFF&bottomColor=%23ffffff&cameraParams=false&varsettransfer=" frameborder="0" id="EmbeddedView-Iframe-96649" allowfullscreen></iframe>

# 🛠 Machines et compétences requises

| Broyeur | Outils nécessaires | Compétences requises |
|---------|--------------------|----------------------|
| <img style="margin-left: 0;" src="../../assets/build/thumb-shredder.jpg" width="100"/> | \- Tour <br> - Perceuse à colonne <br> - Poste à souder (non spécifique) <br> - Ponceuse à bande | \- Soudage (intermédiaire) <br> - Usinage (intermédiaire) <br> - Assemblage (intermédiaire)<br> - Electronique (intermédiaire) |

# ⚡️ Boîtier électronique

Voici une liste des composants électriques à l'intérieur de cette machine. Plus d'informations et de schémas sont à retrouver dans le kit de téléchargement.

* <b>Moteur :</b> les muscles de la machine - recherchez quelque chose avec environ 2,2 kW et une réduction à 70 tr / min.
* <b>Indicateur lumineux :</b> LED s'allumant lorsque la machine est sous tension (souvent située sur l'interrupteur d'alimentation).
* <b>Câble d'alimentation :</b> câble d'alimentation domestique courant.

> Conseil d’expert: Voici un [bon sujet de forum sur l'électronique](https://davehakkens.nl/community/forums/topic/the-big-electronics-topic/)

# 🛠 Trucs et astuces lors de la fabrication

* Bien prendre son temps pour vérifier les angles droits de la boîte avant toute soudure
* Utiliser des écrous de blocage et s’assurer du bon serrage des boulons tenant les roulements

# ♻️ Entrée et sortie

<b>Type :</b> HDPE, LDPE, PP, PS<br> <b>Épaisseur max :</b> 4mm<br> <b>Taille d'entrée trémie :</b> 400 x 200 mm<br> <b>Sortie :</b> ±10 kg/h<br>

### Définition de nos tailles de flocons

Le plastique broyé peut être utilisé dans trois machines Precious Plastic différentes: l’extrudeuse, la presse à injection et la presse à plaques. Nous avons ainsi défini trois tailles de broyage qui fonctionnent avec chaque machine.

| Name: | Large | Medium | Small |
|-------|-------|--------|-------|
| Visual: | <img style="margin-left: 0;" src="../../assets/build/shredder_output_02.jpg" /> | <img style="margin-left: 0;" src="../../assets/build/shredder_output_03.jpg" /> | <img style="margin-left: 0;" src="../../assets/build/shredder_output_04.jpg" /> |
| Size: | 0-30 MM | 0-10 MM | 0-7 MM |
| Works with: | Presse à plaques | Presse à plaques <br> Presse d'injection <br> Compression | Presse à plaques <br> Presse d'injection <br> Compression <br> Extrusion |

# ⚙️ Fonctionnement et maintenance

1. Prêts à broyer ? Ouais ! Choisissez d'abord le type de plastique - cela dépend généralement des quantités de plastique qui s'accumulent dans vos sacs de tri. Assurez-vous que vous avez suffisamment de plastique du même type, car certaines opérations sont requises entre deux cycles de plastiques différents (nettoyage du broyeur par exemple).
2. Le type de plastique a été choisi ? Génial. Il est maintenant temps de réduire les gros objets en plus petits morceaux pouvant aller dans la trémie. On peut faire ça avec un marteau, des ciseaux ou une scie selon le plastique. À ce stade, il faut grossièrement nettoyer les saletés du plastique (il ne faut pas y passer trop de temps, car le plastique sera lavé peu de temps après).
3. Allons-y - introduisez les morceaux obtenus dans la trémie. Pendant le broyage, il convient de régulièrement vérifier les lames et de pousser le plastique vers le bas avec un outil approprié pour s’assurer que les lames saisissent bien les morceaux. Lorsque vous avez fini de travailler avec le broyeur, il faut stocker et étiqueter le plastique broyé ou la prochaine personne utilisant le broyeur ne saura pas quel type de plastique se trouve dans le seau.

<p class="note">Remarque : ⚠️ Il faut être extrêmement prudent lorsqu’on travaille avec le broyeur ! </p>

### Étapes rapides sur l'utilisation du broyeur

1. Rassembler le plastique trié qu’on souhaite broyer
2. Séparer par couleurs
3. Vérifier que le maillage installé a une taille de trou adéquat
4. Allumer la machine
5. Mettre le plastique et attendre
6. Stocker le plastique broyé
7. Nettoyer la machine

### Changer le type de plastique

1. Arrêter la machine, débrancher la prise
2. Retirer le maillage en dessous maintenu par 4 vis
3. Retirer les petits flocons broyés de la machine avec une brosse. Éventuellement souffler avec de l'air comprimé
4. Remettre le maillage et broyer un autre type de plastique

> Conseil : nettoyez les lames et le maillage du broyeur à chaque changement de plastique pour éviter la contamination de différents types de plastique.

# 🔓 Dépannage

1. Parfois, les lames n'agrippent pas le plastique. Dans ce cas, essayez de pousser le plastique vers les lames avec un outil approprié. Ne JAMAIS essayer de faire cela avec vos mains.
2. Si la résistance du plastique dépasse le couple maximum du moteur, la machine s'arrête. Fondamentalement, c’est qu’il y a trop de plastique à broyer pour le moteur. Éteignez alors la machine et retirez une partie du plastique bloqué.

# 🌦 Avantages et inconvénients

| Avantages | Contre |
|-----------|--------|
| Petite et légère | Petite capacité de traitement |
| Possibilité de broyer de petits flocons | Broyage lent |
| Relativement bon marché | Demande beaucoup d’entretien |
| Portable |  |

# 🌎 Construit par la communauté

<div class="j-slideshow">

![Community Shredder](assets/Build/community/machinehack-shredder-4.jpg)

![Community Shredder](assets/Build/community/machinehack-shredder-2.jpg)

![Community Shredder](assets/Build/community/communityshredder1.jpg)

![Community Shredder](assets/Build/community/shredder_community3.jpg)

![Community Shredder](assets/Build/community/shredder_opo.jpg)

![Community Shredder](assets/Build/community/shrednmould.jpg)

</div>

# 🙌 Liens utiles

* [⭐️Mise à niveau : Broyeur 2.1](https://community.preciousplastic.com/how-to/shredder-21)
* [Mise à niveau : efficacité du broyeur](https://davehakkens.nl/community/forums/topic/shredder-efficiency/)
* [Hack : Broyeur & Extrusion](https://davehakkens.nl/community/forums/topic/shredder-and-extrusion-industrial-electronics-2/)
* [Hack : broyeur à propulsion humaine](https://community.preciousplastic.com/how-to/human-powered-shredder)

**Si vous avez besoin d'aide, avez des questions ou cherchez quelqu'un à qui parler dans votre atelier empreint de solitude... Allez sur le canal** [**\#build**](https://discordapp.com/invite/XQDmQVT) **sur Discord. Nous y discutons de la technique entourant la construction des machines.**
