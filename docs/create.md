---
id:create
title:Créer
sidebar_label:Intro

---
<div class="videocontainer"> <iframe width="800" height="400" src="https://www.youtube.com/embed/VdUkOjIP0Ok" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </div>

<style> :root { --highlight: #ffe084; --links: #29bbe3; --hover: rgb(131, 206, 235); } </style>

# Créer

<div class="videoChapters"> <div class="videoChaptersMain">

### Commencer à créer des objets en plastique!

Nous avons donc appris à connaître le plastique, à le collecter et à construire des machines pour le recycler.   
Maintenant, lançons-nous dans la création d’objets de valeur à partir de déchets !   
Comme pour tout autre matériau, travailler avec du plastique demande du temps et du dévouement pour maîtriser la technique. Ne vous précipitez pas. Les premières expériences vont échouer, ce n'est pas grave ! Continuez à essayer. Vous y arriverez !

> Conseil d'expert: *plus votre moule est précis, plus la pièce obtenue sera précise. Prenez le temps de réfléchir à la manière dont vous allez fabriquer le meilleur moule possible en fonction de votre budget et des outils dont vous disposez.*

</div> <div class="videoChaptersSidebar">

### Video Chapters

* 00:00 Introduction
* 00:29 Melting Temps
* 01:33 Make Moulds
* 03:00 Create with Injection
* 04:13 Create with Extrusion
* 06:02 Create with Compression

</div> </div>

Dès les débuts de votre exploration du plastique, vous devez être attentif et comprendre la façon dont il fond, se refroidit, brûle et pourquoi, ou comment le polir et le finir pour obtenir de beaux objets.   
En plus d'acquérir de l'expérience avec le plastique, il est également important de développer un certain degré d'expertise sur les différents matériaux et la façon dont ils fonctionnent avec le plastique.   
C'est particulièrement important si vous voulez fabriquer de nouveaux moules par vous-même.   
Savoir quel matériau conduit la chaleur le plus efficacement, lequel libère le plastique facilement et d'autres informations clés sur les matériaux sera crucial pour votre réussite.

Les informations suivantes regroupent tout ce que vous devez faire avant de fabriquer vos propres nouveaux produits !<br> <br>

![Precious Plastic products](assets/create/pp_products.jpg)

# Apprenez de votre 💩

Parcourez les modules précédents pour apprendre les bases sur les types de plastique et leurs propriétés, les températures de fusion, les machines et leur comportement, et comment ne pas les casser. Plus vous serez préparé, moins vous ferez d'erreurs (croyez-nous, il y en aura beaucoup !).

> Conseil d'expert: Commencez par maîtriser une machine et/ou une technique. Il est facile de s'enthousiasmer et de vouloir commencer par tout, mais il est difficile d'y exceller - concentrez-vous sur une seule, maîtrisez-la et passez à la suivante.   
> C'est la même chose pour le plastique, choisissez un type et concentrez-vous dessus. Réduisez la quantité de variables au début et vous vous remercierez plus tard !

# ❗️ Sécurité

Sécurité. Sécurité. Sécurité. En prenant les précautions appropriées lorsque vous travaillez avec du plastique, vous assurerez votre sécurité en le faisant. Aérez bien la zone et portez toujours un masque.

# 😎 Choisissez votre plastique et commencez doucement

Il y en a beaucoup parmi lesquels choisir et chacun a ses propres propriétés. Certains sont plus faciles à travailler, entre autres parce qu'ils ont une plage de fusion plus longue, et certains peuvent tout simplement être plus disponibles dans votre région que d'autres. 

Si possible, nous vous suggérons de commencer par le PP, le HDPE, le PS car ils sont beaucoup plus faciles à travailler et vous faciliteront grandement la vie.

# 🌡️ Température

Pour recycler un plastique il est impératif de connaître la température de fusion nécessaire pour le fondre sans le brûler. Elle dépendra principalement de deux choses différentes : le type de plastique, et la température ambiante de votre espace de travail (ex : s'il y a du vent, la chaleur se dispersera plus vite). 

Nous avons réalisé [__ce diagramme__](https://onearmy.github.io/academy/assets/plastic/melting-temperatures.jpg) que vous pouvez utiliser comme ressource pour référencer les différents états de fusion de chaque plastique, mais notez une chose importante : Le plastique est souvent mélangé avec des additifs, des pigments et des charges qui transforment sa composition chimique et physique et affectent sa température de fusion finale. Ne soyez pas surpris si parfois la température de fusion recommandée ne permet pas d'atteindre l'état de fusion souhaité, essayez d'ajuster progressivement votre température à la hausse ou à la baisse jusqu'à obtenir la fusion souhaitée.

# 🥵 Ne jamais. Brûler. du Plastique.

Cela peut sembler évident pour certains mais mérite d'être répété.   
Le plastique ne doit jamais être brûlé, à la maison ou en travaillant comme Precious Plastic. Il y a une énorme différence entre faire fondre du plastique et le brûler - les fumées provenant de la combustion du plastique sont hautement toxiques et peuvent causer de graves problèmes de santé aux humains. Pour le processus de recyclage, c'est également une très mauvaise pratique de brûler du plastique car les produits qui en résultent seront endommagés ou de qualité inférieure.   
Tous les plastiques ont une **plage de fusion** ☝️, qui est une fenêtre de température entre laquelle ils fondent (par exemple, 130 à 171 °C). Au-delà de ces températures, le plastique commencera à brûler. Essayez toujours d'éviter cela.

# 💅 les Moules

Les moules font partie intégrante de l'écosystème Precious Plastic et pourraient presque être considérés comme un monde à part entière, ils donnent forme au plastique fondu et créent le produit final.   
Des équipes entières de designers et d'ingénieurs du monde entier consacrent leur vie au développement de moules pour fabriquer la plupart des objets qui nous entourent - votre étui de téléphone, votre chaise d'extérieur, votre pot de fleurs...etc.  
Les moules façonnent notre société moderne, et ils façonneront les produits étonnants que vous créerez ! Les moules peuvent être fabriqués à partir de différents matériaux, mais nous vous conseillons généralement de travailler avec du métal pour sa durabilité et sa meilleure capacité à conduire la chaleur - il est plus efficace sur le plan énergétique.   
Le métal peut être façonné de plusieurs manières, celle que vous utiliserez dépendant du type d'objet que vous avez l'intention de fabriquer et des technologies auxquelles vous avez accès.

> Conseil d'expert: apprenez à connaître les différentes techniques de moulage disponibles afin de pouvoir concevoir en fonction des points forts de chaque méthode. 
>
> Vous pouvez adopter une approche plus pratique et créer de nouveaux moules à l'aide d'une machine à souder (qui est un peu plus brutale), d'une fraiseuse ou d'un tour.   
> Vous pouvez aussi créer un fichier CAO et utiliser des technologies plus avancées (et plus coûteuses) comme le fraisage CNC, qui est très précis.

![Socket Mould](assets/create/sockets.jpg)

Notez qu'il existe de nombreuses façons d'explorer le moulage et ses différentes méthodes, en utilisant par exemple des moules en bois, en plastique, imprimés en 3d ou moulés en aluminium.   
Nous vous conseillons de prendre le temps de réfléchir, de concevoir et de fabriquer un moule précis afin de créer de meilleurs produits. Cela sera payant à long terme !

<b>Vous voulez voir certains des meilleurs moules de la communauté ?</b>

* [Fabrication d'un handplane de surf - moule  simple ](https://community.preciousplastic.com/how-to/make-a-handplane-simple-mould)<br>
* [Make a broom hanger](https://community.preciousplastic.com/how-to/make-a-broom-hanger)<br>
* [Make a carabiner CNC vs laser cut](https://community.preciousplastic.com/how-to/make-a-carabiner-cnc-vs-lasercut)<br>
* [Make an interlocking brick](https://community.preciousplastic.com/how-to/make-an-interlocking-brick)

# 💰  Donner de la valeur au plastique

Nous pensons que le plastique ne doit pas être considéré comme jetable ou bon marché. Nous nous efforçons de donner de la valeur au plastique et de créer des objets qui durent longtemps et que les gens chériront. Nous espérons que les designers, les ingénieurs, les entrepreneurs et les personnes de la communauté verront le plastique comme un matériau précieux pour créer et acheter des objets beaux et utiles.   
Faites des efforts dans vos produits pour que les gens les achètent parce qu'ils en ont envie, et non parce que c'est bon marché, et nous réaliserons tous quelque chose de formidable : le plastique ne finira pas dans l'environnement.

# 🤝 Partager

Vous avez fabriqué un produit sympa ? Vous avez trouvé le moyen de rationaliser votre production ? Vous avez fait fabriquer un excellent moule ? Notre communauté mondiale (vous !) est l'épine dorsale de Precious Plastic et ensemble, nous trouvons chaque jour de nouvelles solutions pour résoudre le problème du plastique.   
N'oubliez pas de publier un How-To, ou de poster dans le Discord de la communauté. Aidez à faire avancer le projet !

# Restez ouvert et collaboratif

Nous nous sommes occupés de concevoir les outils, les ressources et le cadre dont vous avez besoin pour gérer un espace de travail de recyclage de plastique précieux, et maintenant que vous avez toutes les connaissances, c'est à vous de le faire fonctionner dans votre communauté locale et de le faire passer au niveau supérieur.   
  
Que vous souhaitiez créer des produits, fabriquer des moules, pirater les machines, collaborer avec d'autres designers/ingénieurs, expérimenter, enseigner aux autres, organiser des collectes, quel que soit votre choix, assurez-vous de garder l'esprit Precious Plastic avec vous : soyez ouvert, soyez collaboratif.

<b>Vous voulez partager vos réactions, discuter des produits/de leur conception ou en apprendre davantage sur la communauté ? Rendez-vous sur le canal [**__\#Create__**](https://discordapp.com/invite/yhmfzTZ) sur Discord. Nous y parlons de la conception des produits, de la fabrication des moules, des mélanges de couleurs, de la finition... tout pour créer des objets précieux !</b>
